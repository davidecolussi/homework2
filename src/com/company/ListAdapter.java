package com.company;

import java.util.*; //uso java.util versione J2SE per realizzare l'adapter da Target List a Adaptee Vector

import static org.junit.Assert.*;

public class ListAdapter<E> implements List<E> { //implementazione con objectAdapter, questa list ammette valori null
    Vector<E> v;

    /**
     * initialize this list, initial size is zero
     *
     */

    public ListAdapter(){
        v = new Vector(1);  //inizialmente vettore con capacità 1
    }

    /**
     * Inserts the specified element at the specified position in this list (optional operation). Shifts the element currently at that position (if any) and any subsequent elements to the right (adds one to their indices).
     * @param index the index where to add the element
     * @param element the element to add
     * @throws IndexOutOfBoundsException if the index is out of range
     */

    public void add(int index, E element){
        try {
            v.insertElementAt(element, index);
            assertEquals(v.elementAt(index), element);

        }catch (ArrayIndexOutOfBoundsException e){
            IndexOutOfBoundsException ioob = new IndexOutOfBoundsException();
            ioob.initCause(e);
            throw ioob;
        }
    }

    /**
     * appends an element at the end of the list
     * @param e the element to be added in this list
     * @return true if the size of the list has changed
     */

    public boolean add(E e) {
        int oldSize = v.size();
        v.addElement(e);

        if(v.size() > oldSize)
            return true;
        return false;
    }

    /**
     * Inserts all of the elements in the specified collection into this list at the specified position (optional operation).
     * @param index the index where to add the elements
     * @param cl the collection
     * @return true if this list changed as a result of the call
     */

    public boolean addAll(int index, Collection<? extends E> cl){
        Object[] arr = cl.toArray();
        int oldSize = size();

        for(int i = 0 ; i < arr.length; i++)
            add(index + i, (E)arr[i]);

        return (size()!=oldSize);
    }

    /**
     * Appends all of the elements in the specified collection to the end of this list, in the order that they are returned by the specified collection's iterator (optional operation).
     * @param c the collection
     * @return true if this list changed as a result of the call
     */
    public boolean addAll(Collection<? extends E> c){
        Object[] arr = c.toArray();
        int oldSize = size();

        for(int i = 0 ; i<arr.length; i++)
            add((E)arr[i]);

        return(size()!=oldSize);
    }

    /**
     * Removes all of the elements from this list (optional operation)
     */

    public void clear(){
        v.removeAllElements();
    }


    /**
     * Tests if the specified object appears in this list
     * @param o the object to search in this list
     * @return true if specified object appears in this list
     * @throws NullPointerException if o is null
     */
    public boolean contains(Object o){
        if(o == null)
            throw new NullPointerException();
        return v.contains(o);
    }

    /**
     * Returns true if this sublist contains all of the elements of the specified collection.
     * @param  c collection to be checked for containment in this collection
     * @return true if this collection contains all of the elements in the specified collection
     * @throws NullPointerException - if the specified collection is null.
     */

    public boolean containsAll(Collection<?> c){
        if(c == null)
            throw new NullPointerException();

        Object[] arr = c.toArray();

        for(int i = 0; i<arr.length; i++){
            if(!contains(arr[i]))
                return false;
        }
        return true;
    }

    /**
     * Compares the specified object with this list for equality
     * @param o object to be compared for equality with this collection
     * @return true if every element in the o list is present in this list in the same order
     */

    public boolean equals(Object o){
        ListAdapter<E> other = (ListAdapter<E>)o;

        if(other.size() == size()){
            for(int i = 0 ; i < size(); i++){
                if(!get(i).equals(other.get(i)))
                    return false;   //an element is different
            }
            return true;    //all element are equals
        }
        else
            return false;   //different sizes
    }


    /**
     * Return the element at the specified position in this list
     * @param index the specified position
     * @return the object at the specified index
     * @throws IndexOutOfBoundsException if an invalid index was given
     */
    public E get(int index){
        try {
            return v.elementAt(index);  //può lanciare arrayioobexc
        }catch (ArrayIndexOutOfBoundsException e){
            IndexOutOfBoundsException ioobe = new IndexOutOfBoundsException();
            ioobe.initCause(e);
            throw ioobe;
        }
    }

    /**
     * Returns the hash code value for this list
     * @return the hash code value for this list calculated as (for each obj in this list) hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode())
     */
    public int hashCode(){
        int hashCode = 1;

        Iterator i = iterator();
        while (i.hasNext()) {
            Object obj = i.next();
            hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
        }
        return hashCode;
    }

    /**
     * Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
     * @param o the object to search in the list
     * @return the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
     */

    public int indexOf(Object o){
        return v.indexOf(o);
    }

    /**
     * return true if the list is empty
     * @return true if the list is empty
     */
    public boolean isEmpty(){
        return v.isEmpty();
    }

    /**
     * Return the iterator of the list
     * @return the iterator of the list starting from the first element
     */

    public Iterator<E> iterator(){
        return new Iterator<E>() {
            int curr = 0;

            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                return get(curr++);
            }
        };
    }

    /**
     * Sorts this list according to the order induced by the specified Comparator.
     * @param c the comparator
     */
    public void sort(Comparator<? super E> c){
        //selection sort

        for(int i = 0; i < size()-1; i++) {
            int minimo = i; //Partiamo dall' i-esimo elemento
            for(int j = i+1; j < size(); j++) {
                //Qui avviene la selezione, ogni volta
                //che nell' iterazione troviamo un elemento piú piccolo di minimo
                //facciamo puntare minimo all' elemento trovato
                if(c.compare(get(minimo),get(j)) > 0) {
                    minimo = j;
                }
            }
            //Se minimo e diverso dall' elemento di partenza
            //allora avviene lo scambio
            if(minimo != i) {
                E k = get(minimo);
                set(minimo,get(i));
                set(i,k);
            }
        }
    }



    /**
     * Returns the index of the last occurrence of the specified object in this vector.
     * @param o the object to search in the list
     * @return the index of the last occurrence of the specified object in this vector; returns -1 if the object is not found.
     */
    public int lastIndexOf(Object o){
        return v.lastIndexOf(o);
    }


    /**
     * Returns a list iterator over the elements in this list (in proper sequence).
     * @return the list iterator over the elements in this list starting from the first element
     */
    public ListIterator<E> listIterator(){
        return new ListIterator<E>() {
            int curr = 0;
            boolean precSet = false;
            boolean nextSet = false;


            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                nextSet = true;
                precSet = false;
                return(get(curr++));
            }

            @Override
            public boolean hasPrevious() {
                if(curr > 0)
                    return true;
                return false;
            }

            @Override
            public E previous() {
                if(!hasPrevious())
                    throw new NoSuchElementException();
                precSet = true;
                nextSet = false;
                return(get(curr--));
            }

            @Override
            public int nextIndex() {
                return(curr);
            }

            @Override
            public int previousIndex() {
                return(curr-1);
            }

            @Override
            public void remove() {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    ListAdapter.this.remove(curr-1);
                    nextSet=false;
                }
                else if(precSet){
                    ListAdapter.this.remove(curr+1);
                    precSet = false;
                }
            }

            @Override
            public void set(E e) {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    ListAdapter.this.set(curr-1,e);
                    nextSet=false;
                } else if (precSet) {
                    ListAdapter.this.set(curr+1,e);
                    precSet=false;
                }
            }

            @Override
            public void add(E e) {
                v.insertElementAt(e,curr++);
                nextSet = false;
                precSet = false;
            }
        };
    }

    /**
     * Returns a list iterator over the elements in this list (in proper sequence), starting at the specified position in the list.
     * @param index the starting index
     * @return the list iterator over the elements in this list (in proper sequence), starting at the specified position in the list.
     */

    public ListIterator<E> listIterator(int index){
        return new ListIterator<E>() {
            int curr = index;   //listIterator starting from the specified index
            boolean precSet = false;
            boolean nextSet = false;


            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                nextSet = true;
                precSet = false;
                return(get(curr++));
            }

            @Override
            public boolean hasPrevious() {
                if(curr > 0)
                    return true;
                return false;
            }

            @Override
            public E previous() {
                if(!hasPrevious())
                    throw new NoSuchElementException();
                precSet = true;
                nextSet = false;
                return(get(curr--));
            }

            @Override
            public int nextIndex() {
                return(curr);
            }

            @Override
            public int previousIndex() {
                return(curr-1);
            }

            @Override
            public void remove() {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    ListAdapter.this.remove(curr-1);
                    nextSet=false;
                }
                else if(precSet){
                    ListAdapter.this.remove(curr+1);
                    precSet = false;
                }
            }

            @Override
            public void set(E e) {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    ListAdapter.this.set(curr-1,e);
                    nextSet=false;
                } else if (precSet) {
                    ListAdapter.this.set(curr+1,e);
                    precSet=false;
                }
            }

            @Override
            public void add(E e) {
                v.insertElementAt(e,curr++);
                nextSet = false;
                precSet = false;
            }
        };
    }

    /**
     * removes the element at the given index
     * @param index the index which object associated has to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException - if the index is out of range
     */

    public E remove(int index){
        E result = v.elementAt(index);
        try{
            v.removeElementAt(index);
        }catch (ArrayIndexOutOfBoundsException e){
            IndexOutOfBoundsException ioob = new IndexOutOfBoundsException();
            ioob.initCause(e);
            throw ioob;
        }
        return result;
    }

    /**
     * Removes the first occurrence of the specified element from this list, if it is present (optional operation).
     * @param o the element to be removed
     * @return true if this list contained the specified element
     */
    public boolean remove(Object o){
        int index = indexOf(o);

        if(index == -1)
            return false;    //element wasn't present
        else{
            remove(index);
            return true;
        }
    }

    /**
     * Removes from this list all of its element that are contained in the specified collection (optional operation)
     * @param c collection containing elements to be removed from this collection
     * @return true if this list changed as a result of the call
     */
    public boolean removeAll(Collection<?> c){
        Object[] arr = c.toArray();
        int oldSize = size();

        for(int i = 0 ; i< arr.length; i++)
            remove(arr[i]);

        return(size()!=oldSize);
    }


    /**
     * Retains only the elements in this list that are contained in the specified collection (optional operation). In other words, removes from this list all the elements that are not contained in the specified collection.
     * @param c collection containing elements to be retained in this collection
     * @return true if this list changed as a result of the call.
     */

    public boolean retainAll(Collection<?> c){
        int oldSize = size();

        if(c.size() == 0){
            v.clear();
        }

        else{
            Vector<E> tempList = new Vector<>();
            Object[] arr = c.toArray();

            for(int i = 0 ; i < arr.length; i++){   //cycles through the collection
                //if an element of the collection is contained in the list, the element is added in the tempList
                if(contains(arr[i]))
                    tempList.add((E)arr[i]);
            }

            v = tempList;   //overwrite the current list with the tempList created
        }

        return (size()!=oldSize);
    }

    /**
     * Replaces the element at the specified position in this list with the specified element (optional operation).
     * @param index the index of the list
     * @param element the new element
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException if the index is out of the range of the list
     */

    public E set(int index, E element){
        try {
            E result = get(index);
            v.setElementAt(element, index);
            return result;
        }catch (ArrayIndexOutOfBoundsException e){
            IndexOutOfBoundsException ioob = new IndexOutOfBoundsException();
            ioob.initCause(e);
            throw ioob;
        }
    }

    /**
     *returns the size of the list
     */

    public int size(){
        return v.size();
    }


    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @param fromIndex starting index (inclusive)
     * @param toIndex finishing index (exclusive)
     * @return a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @throws IndexOutOfBoundsException - for an illegal endpoint index value
     * @see SubList
     */

    public List<E> subList(int fromIndex, int toIndex){
        if(fromIndex < 0 || toIndex > size() || fromIndex > toIndex)
            throw new IndexOutOfBoundsException();

        SubList<E> list = new SubList<>(this, fromIndex,toIndex);
        return list;
    }

    /**
     * Returns an array containing all of the elements in this list in proper sequence (from first to last element)
     */

    public Object[] toArray(){
        return v.toArray();
    }

}
