package com.company;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ListAdapterTest<E> {

    public ListAdapter<E> getNewPopulatedList(){ //classe utile per altri test... ritorna un'istanza della lista popolata
        ListAdapter<E> lista = new ListAdapter<E>();

        String s = new String("prova");
        lista.add((E)s);
        String s2 = new String("abc");
        lista.add((E)s2);
        String s3 = new String("cde");
        lista.add((E)s3);

        return lista;
    }

    @Test
    public void testAdd(){
        ListAdapter<E> lista = new ListAdapter<E>();

        Vector<String> vprova = new Vector<>();
        vprova.add("prova0");
        vprova.add("prova1");
        vprova.add("prova2");

        for(int i = 0 ; i<3 ;i++){
            E input =(E) new String("prova" + (i));
            lista.add(input);
        }

        assertEquals(vprova.toArray(),lista.toArray());
    }

    @Test
    public void testAdd2(){ //listAdapter should allow null elements
        ListAdapter<E> lista = getNewPopulatedList();
        boolean result = lista.add(null);
        assertTrue(result);
    }


    /**
     * ensure that add adds an object to the list in the correct index
     */

    @Test
    public void testAddIndex1(){
        ListAdapter<E> lista = new ListAdapter<E>();

        E elem = (E)new String("prova");
        lista.add(0,elem);
        Object result = lista.get(0);   //da sostituire con get della ListAdapter
        assertEquals(elem,result);
    }

    @Test
    public void testAddIndex2(){
        ListAdapter<E> lista = getNewPopulatedList();

        lista.add(0,(E)"nuovo");

        assertEquals("nuovo",lista.get(0));
    }

    @Test
    public void testAddIndex3(){
        ListAdapter<E> lista = getNewPopulatedList();

        lista.add(1,(E)"nuovo");

        assertEquals("prova",lista.get(0));
        assertEquals("nuovo",lista.get(1));
        assertEquals("abc",lista.get(2));
        assertEquals("cde",lista.get(3));
    }

    @Test
    public void testAddIndex4(){
        ListAdapter<E> lista = getNewPopulatedList();

        lista.add(3,(E)"nuovo");

        assertEquals("prova",lista.get(0));
        assertEquals("abc",lista.get(1));
        assertEquals("cde",lista.get(2));
        assertEquals("nuovo",lista.get(3));
    }

    /**
     * ensure that add(int index, E elem) doesn't add an object out of bounds
     */

    @Test
    public void testAddOutOfBounds(){
        ListAdapter<E> lista = new ListAdapter<E>();

        E elem = (E)new String("prova2");
        try{
            lista.add(lista.size()+1,elem);
            Object result = lista.get(lista.size()+1);
            assertEquals(elem,result);
        }catch(IndexOutOfBoundsException e){
        }
    }

    @Test
    public void testInitialSize(){
        ListAdapter<E> lista = new ListAdapter<E>();
        assertEquals(0,lista.size());
    }

    @Test
    public void testSize(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        lista.add((E)s);
        assertEquals(lista.size(),1);
    }

    @Test
    public void testClear(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        lista.add((E)s);
        lista.clear();
        assertEquals(0,lista.size());
    }

    @Test
    public void testGet1(){
        ListAdapter<E> lista = getNewPopulatedList();
        assertEquals("cde",lista.get(2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGet2(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.get(3); //out of bounds (3>=size)
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGet3(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.get(-1); //(-1 is negative))
    }

    @Test
    public void testContains1(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        lista.add((E)s);
        assertTrue(lista.contains(s));
    }

    @Test
    public void testContains2(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        assertFalse(lista.contains(s));
    }

    @Test
    public void testIsEmpty1(){
        ListAdapter<E> lista = new ListAdapter<E>();
        assertTrue(lista.isEmpty());
    }

    @Test
    public void testIsEmpty2(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        lista.add((E)s);
        assertFalse(lista.isEmpty());
    }

    @Test
    public void testIsEmpty3(){
        ListAdapter<E> lista = new ListAdapter<E>();
        String s = new String("prova");
        lista.add((E)s);
        lista.clear();
        assertTrue(lista.isEmpty());
    }

    @Test
    public void testRemove1(){
        ListAdapter<E> lista = getNewPopulatedList();

        E result = lista.remove(0);
        assertEquals("prova",result);
        assertEquals(2, lista.size());
    }

    @Test
    public void testRemove2(){
        ListAdapter<E> lista = getNewPopulatedList();
        E result = lista.remove(0);
        assertNotEquals("abc",result);
    }

    @Test
    public void testRemove3(){
        ListAdapter<E> lista = new ListAdapter<>();
        lista.add((E)"nuovo");
        lista.remove(0);

        assertEquals(0,lista.size());
    }

    @Test
    public void testRemove4(){
        ListAdapter<E> lista = new ListAdapter<>();
        lista.add((E)"nuovo");
        lista.add((E)"nuovo2");

        lista.remove(0);
        lista.remove(0);

        assertEquals(0,lista.size());
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemove5(){  //check indexOutOfBound is thrown if index >= size
        ListAdapter<E> lista = getNewPopulatedList();

        lista.remove(0);
        lista.remove(0);
        lista.remove(0);
        //now list is empty
        lista.remove(0); //exception must be thrown
    }


    @Test
    public void testIndexOf1(){
        ListAdapter<E> lista = getNewPopulatedList();
        int index = lista.indexOf("abc");
        assertEquals(1,index);
    }

    @Test
    public void testIndexOf2(){
        ListAdapter<E> lista = getNewPopulatedList();
        int index = lista.indexOf("inesistente");
        assertEquals(-1,index);
    }

    @Test
    public void testRemoveObject1(){
        ListAdapter<E> lista = getNewPopulatedList();
        boolean result = lista.remove("abc");
        assertTrue(result);
    }

    @Test
    public void testRemoveObject2(){
        ListAdapter<E> lista = getNewPopulatedList();
        boolean result = lista.remove("inesistente");
        assertFalse(result);
    }

    @Test
    public void testLastIndexOf1(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.add((E)"abc");  //la stringa abc compare già nell'indice 1, ore anche nell'indice 3

        assertEquals(3,lista.lastIndexOf("abc"));
    }

    @Test
    public void testLastIndexOf2(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.add((E)"prova");  //la stringa prova compare già nell'indice 0, ore anche nell'indice 3

        assertEquals(3,lista.lastIndexOf("prova"));
    }

    @Test
    public void testLastIndexOf3(){
        ListAdapter<E> lista = getNewPopulatedList();
        assertEquals(0,lista.lastIndexOf("prova"));
    }

    @Test
    public void testSet1(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.set(1,(E)"nuovastringa");
        assertEquals(lista.get(1),"nuovastringa");
        assertEquals(3,lista.size()); //check that the size isn't changed
    }

    @Test
    public void testSet2(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.set(2,(E)"nuovastringa");
        assertEquals("nuovastringa",lista.get(2));
    }

    @Test
    public void testSet3(){
        ListAdapter<E> lista = getNewPopulatedList();
        lista.set(2,(E)"cde");   //set the same element again
        assertEquals(lista.get(2),"cde");
    }

    @Test
    public void testToArray1(){
        ListAdapter<E> lista = new ListAdapter<E>();

        //caso in cui la lista è vuota
        Object[] arr = lista.toArray();
        assertEquals(0,arr.length);
    }

    @Test
    public void testToArray2(){
        //caso in cui la lista è popolata
        ListAdapter<E> lista = getNewPopulatedList();
        Object[] arr = lista.toArray();
        assertEquals(3,arr.length);
    }

    @Test
    public void testToArray3(){
        //caso in cui la lista è popolata
        ListAdapter<E> lista = getNewPopulatedList();
        Object[] arr = lista.toArray();
        assertEquals("abc",arr[1]);
    }

    @Test
    public void testEquals1(){
        ListAdapter<E> lista = getNewPopulatedList();

        assertEquals(lista,lista);
    }

    @Test
    public void testEquals2(){
        ListAdapter<E> lista = getNewPopulatedList();
        ListAdapter<E> lista2 = getNewPopulatedList();

        lista2.add((E)"differente");
        //lists have different sizes
        assertNotEquals(lista,lista2);
    }

    @Test
    public void testEquals3(){
        ListAdapter<E> lista = getNewPopulatedList();
        ListAdapter<E> lista2 = getNewPopulatedList();

        lista2.set(2,(E)"differente");
        //list have same size but an element differs
        assertNotEquals(lista,lista2);
    }

    @Test
    public void testEquals4(){
        ListAdapter<E> lista = getNewPopulatedList();
        ListAdapter<E> lista2 = getNewPopulatedList();

        lista2.set(2,(E)"differente");
        //list have same size but an element differs
        lista2.set(2,(E)"cde");
        //now lists should be equals again
        assertEquals(lista,lista2);
    }


    @Test
    public void testListaInteger(){ //testa se la lista funziona con gli integer
        ListAdapter<E> lista = new ListAdapter<>();

        Integer i = 10;
        lista.add((E)i);

        assertEquals(lista.get(0),10);
    }

    @Test
    public void testSort(){
        ListAdapter<Integer> lista = new ListAdapter<>();

        lista.add(1);
        lista.add(5);
        lista.add(3);
        lista.add(4);
        lista.add(2);

        lista.sort(Integer::compareTo);

        ListAdapter<Integer> listaOrdinataManualmente = new ListAdapter<>();
        listaOrdinataManualmente.add(1);
        listaOrdinataManualmente.add(2);
        listaOrdinataManualmente.add(3);
        listaOrdinataManualmente.add(4);
        listaOrdinataManualmente.add(5);

        assertEquals(listaOrdinataManualmente,lista);
    }

    @Test
    public void testIterator1(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        assertEquals(iteratore.next(),"prova");
    }

    @Test(expected = NoSuchElementException.class)
    public void testIterator2(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        iteratore.next();
        iteratore.next();
        iteratore.next();
        iteratore.next();
    }

    @Test
    public void testIterator3(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        iteratore.next();
        iteratore.next();
        assertEquals(iteratore.next(),"cde");
    }



    @Test
    public void testIterator4(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        assertTrue(iteratore.hasNext());
    }


    @Test
    public void testIterator5(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        iteratore.next();
        iteratore.next();
        iteratore.next();

        assertFalse(iteratore.hasNext());
    }

    @Test
    public void testIterator6(){
        ListAdapter<E> lista = getNewPopulatedList();
        Iterator<E> iteratore = lista.iterator();

        iteratore.next();
        iteratore.next();

        assertTrue(iteratore.hasNext());
    }


    @Test
    public void testListIterator1(){
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        assertEquals("prova", listIterator.next());
    }



    @Test
    public void testListIterator2(){    //check che scorrendo l'iteratore venga fuori ciò che sta nella lista
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        int i = 0;
        boolean result = true;

        while(listIterator.hasNext()){
            E elem = listIterator.next();
            if(!(lista.get(i).equals(elem)))
                result = false;
            i++;
        }

        assertTrue(result);
    }


    @Test
    public void testListIterator3(){    //check che hasNext sia settato giusto
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        int i = 0;

        while(listIterator.hasNext()) {
            listIterator.next();
            i++;
        }
        assertEquals(lista.size(),i);
    }

    @Test(expected = NoSuchElementException.class)
    public void testListIterator4(){    //check that NoSuchElement is thrown
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        while(listIterator.hasNext()) {
            listIterator.next();
        }

        listIterator.next(); //exc thrown
    }

    @Test(expected = NoSuchElementException.class)
    public void testListIterator5(){    //check that NoSuchElement is thrown
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        listIterator.previous(); //exc thrown
    }

    @Test
    public void testListIterator6(){    //check that hasPrevious is initially false
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        assertFalse(listIterator.hasPrevious());
    }

    @Test
    public void testListIterator7(){    //check that nextIndex is initially 0
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        assertEquals(0,listIterator.nextIndex()); //exc thrown
    }



    @Test
    public void testListIterator8(){    //check that previousIndex is initially -1
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        assertEquals(-1,listIterator.previousIndex()); //exc thrown
    }

    @Test
    public void testListIterator9(){    //check that previousIndex is correctly updated
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        listIterator.next();

        assertEquals(0,listIterator.previousIndex()); //exc thrown
    }

    @Test
    public void testListIterator10(){    //check that nextIndex is correctly updated
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        listIterator.next();

        assertEquals(1,listIterator.nextIndex()); //exc thrown
    }

    @Test(expected = IllegalStateException.class)
    public void testListIterator11(){    //check that remove() can't be called without calling next()/previous() first
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        listIterator.remove();//exc thrown
    }

    @Test(expected = IllegalStateException.class)
    public void testListIterator12(){    //check that remove() behave correctly (correctly remove the first element and shifts left the others)
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        listIterator.next();
        listIterator.remove();

        assertEquals("abc",lista.get(0));
        assertEquals("cde",lista.get(1));
        assertEquals(2,lista.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testListIterator13(){    //check that remove() can't be called without calling next() before
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        listIterator.next();
        listIterator.remove();
        listIterator.remove();
    }

    @Test
    public void testListIterator14(){    //check that add() behaves correctly at the first element
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        E newElem = (E)new String("nuovo");
        listIterator.add(newElem);

        assertEquals("nuovo",lista.get(0));
        //from there it must be equal to the old lista
        assertEquals("prova",lista.get(1));
        assertEquals("abc",lista.get(2));
        assertEquals("cde",lista.get(3));
        assertEquals(4,lista.size());
    }

    @Test
    public void testListIterator15(){    //check that add() behaves correctly at the last element
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();

        E newElem = (E)new String("nuovo");
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.add(newElem);

        //from there it must be equal to the old lista
        assertEquals("prova",lista.get(0));
        assertEquals("abc",lista.get(1));
        assertEquals("cde",lista.get(2));
        assertEquals("nuovo",lista.get(3));

        assertEquals(4,lista.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testListIterator16(){    //check that set() can't be called without calling next()/previous() before
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        E newElem = (E)new String("nuovo");

        listIterator.set(newElem);
    }

    @Test(expected = IllegalStateException.class)
    public void testListIterator17(){    //check that set() behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator();
        E newElem = (E)new String("nuovo");

        listIterator.next();
        listIterator.set(newElem);

        assertEquals("nuovo",lista.get(1));
    }


    @Test
    public void testListIterator18(){    //check that hasPrevious() behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        ListIterator<E> listIterator = lista.listIterator();

        listIterator.next();

        assertTrue(listIterator.hasPrevious());
    }



    @Test
    public void testListIteratorFromIndex1(){    //check that listIterator(index) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator(0);

        assertEquals("prova",listIterator.next());
    }

    @Test
    public void testListIteratorFromIndex2(){    //check that listIterator(index) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        ListIterator<E> listIterator = lista.listIterator(1);

        assertEquals("abc",listIterator.next());
    }
    //other tests on listIteratorFromIndex should behave as the tests already made on listIterator (without index)


    @Test
    public void testContainsAll1(){  //check that containsAll behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");
        coll.add((E)"prova");
        coll.add((E)"cde");

        assertTrue(lista.containsAll(coll));
    }

    @Test
    public void testContainsAll2(){  //check that containsAll behaves correctly 2
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"abc");
        coll.add((E)"differente");
        coll.add((E)"cde");

        assertFalse(lista.containsAll(coll));
    }

    @Test
    public void testContainsAll3(){  //check that containsAll behaves correctly 3 (different sizes)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");
        coll.add((E)"cde");

        assertTrue(lista.containsAll(coll));
    }

    @Test
    public void testContainsAll4(){  //check that containsAll behaves correctly 4 (different sizes)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");
        coll.add((E)"differente");

        assertFalse(lista.containsAll(coll));
    }

    @Test
    public void testContainsAll5(){  //check that containsAll behaves correctly 5 (different sizes)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"differente");
        coll.add((E)"differente");

        assertFalse(lista.containsAll(coll));
    }

    @Test
    public void testContainsAll6(){  //check that containsAll behaves correctly 5 (type different from string)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)new Integer(5));
        coll.add((E)"abc");

        assertFalse(lista.containsAll(coll));
    }

    @Test
    public void removeAlltest1(){   //check that removeAll behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");

        lista.removeAll(coll);

        assertFalse(lista.contains("abc"));
    }

    @Test
    public void removeAlltest2(){   //check that removeAll behaves correctly 2
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");
        coll.add((E)"prova");

        lista.removeAll(coll);

        assertFalse(lista.contains("abc"));
        assertFalse(lista.contains("prova"));
    }

    @Test
    public void removeAlltest3(){   //check that removeAll returns true if the list has changed
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");

        boolean result = lista.removeAll(coll);

        assertTrue(result);
    }

    @Test
    public void removeAlltest4(){   //check that removeAll returns false if the list hasn't changed
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        //coll does not contain any elements, calling lista.removeAll(call) should return false

        boolean result = lista.removeAll(coll);

        assertFalse(result);
    }

    @Test
    public void removeAlltest5(){   //check that removeAll returns false if the list hasn't changed
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"differente");
        //coll does not contain any elements of list, calling lista.removeAll(call) should return false

        boolean result = lista.removeAll(coll);

        assertFalse(result);
    }

    @Test
    public void testAddAll1(){  //check that addAll behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo1");
        coll.add((E)"nuovo2");

        lista.addAll(coll);

        assertTrue(lista.contains("nuovo1"));
        assertTrue(lista.contains("nuovo2"));
    }

    @Test
    public void testAddAll2(){  //check that addAll doesn't remove old elements contained in the list
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo1");

        lista.addAll(coll);

        assertTrue(lista.contains("prova"));
        assertTrue(lista.contains("abc"));
        assertTrue(lista.contains("cde"));
    }

    @Test
    public void testAddAll3(){  //check that addAll behaves correctly (empty collection)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        //coll is empty, lista.addAll shouldn't modify the list

        boolean result = lista.addAll(coll);

        assertFalse(result);
    }

    @Test
    public void testAddAll4(){  //check that addAll behaves correctly (returns true if the list was modified)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo");

        boolean result = lista.addAll(coll);

        assertTrue(result);
    }

    @Test
    public void addAllFromIndexTest1(){ //check that addAll(index,elem) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo");

        lista.addAll(0,coll);   //inserts at the start of the list

        assertEquals("nuovo",lista.get(0));
    }

    @Test
    public void addAllFromIndexTest2(){ //check that addAll(index,elem) behaves correctly 2
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo");

        lista.addAll(1,coll);   //inserts at the index 1 of the list

        assertEquals("nuovo",lista.get(1));
    }

    @Test
    public void addAllFromIndexTest3(){ //check that addAll(index,elem) behaves correctly 3 (shifts all the element to the right)
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo");

        lista.addAll(1,coll);   //inserts at the index 1 of the list

        assertEquals("nuovo",lista.get(1));
        assertEquals("prova",lista.get(0));
        assertEquals("abc",lista.get(2));
        assertEquals("cde",lista.get(3));
    }

    @Test
    public void addAllFromIndexTest4(){ //check that addAll(index,elem) returns true if the list has changed
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"nuovo");

        boolean result = lista.addAll(0,coll);   //inserts at the index 0 of the list

        assertTrue(result);
    }

    @Test
    public void addAllFromIndexTest5(){ //check that addAll(index,elem) returns false if the list hasn't changed
        ListAdapter<E> lista = getNewPopulatedList();

        Collection<E> coll = new ListAdapter<E>();
        //coll is empty, nothing to add

        boolean result = lista.addAll(0,coll);   //inserts nothing at the index 1 of the list

        assertFalse(result);
    }

    @Test
    public void testRetainAll1(){
        ListAdapter<E> lista = getNewPopulatedList();      //check retainAll retains one element from the list

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");

        lista.retainAll(coll);  //only abc should be preserved

        assertTrue(lista.contains("abc"));
        assertEquals(1, lista.size());
    }

    @Test
    public void testRetainAll2(){
        ListAdapter<E> lista = getNewPopulatedList();      //check retainAll retains none elements of the list

        Collection<E> coll = new ListAdapter<E>();
        //void collection

        lista.retainAll(coll);  //nothing should be preserved from the list

        assertEquals(0, lista.size());
    }

    @Test
    public void testRetainAll3(){
        ListAdapter<E> lista = getNewPopulatedList();      //check retainAll retains two elements from the list

        Collection<E> coll = new ListAdapter<E>();
        coll.add((E)"abc");
        coll.add((E)"prova");

        lista.retainAll(coll);  //two elements should be preserved from the list

        assertEquals(2, lista.size());
        assertTrue(lista.containsAll(coll));
        assertFalse(lista.contains("cde"));
    }

    //metodo utile per stampare una lista
    public void print(ListAdapter<E> lista){
        for(int i = 0 ; i<lista.size(); i++)
            System.out.println("Pos " + i + ": " + lista.get(i));
    }
}
