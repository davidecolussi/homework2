package com.company;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface Collection<E>{

    /**
     * add element
     *
     * @param e element to add
     * @return true if this collection changed as a result of the
     *         call
     * @throws UnsupportedOperationException if the add operation
     *         is not supported by this collection
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this collection
     * @throws NullPointerException if the specified element is null and this
     *         collection does not permit null elements
     * @throws IllegalArgumentException if some property of the element
     *         prevents it from being added to this collection
     * @throws IllegalStateException if the element cannot be added at this
     *         time due to insertion restrictions
     */
    boolean add(E e);

    /**
     * Adds all of the elements in the specified collection to this collection
     *
     *
     * @param c collection containing elements to be added to this collection
     * @return true if this collection changed as a result of the call
     * @throws UnsupportedOperationException if the addAll operation
     *         is not supported by this collection
     * @throws ClassCastException if the class of an element of the specified
     *         collection prevents it from being added to this collection
     * @throws NullPointerException if the specified collection contains a
     *         null element and this collection does not permit null elements,
     *         or if the specified collection is null
     * @throws IllegalArgumentException if some property of an element of the
     *         specified collection prevents it from being added to this
     *         collection
     * @throws IllegalStateException if not all the elements can be added at
     *         this time due to insertion restrictions
     * @see #add(Object)
     */
    boolean addAll(Collection<? extends E> c);

    /**
     * Removes all of the elements from this collection (optional operation).
     * The collection will be empty after this method returns.
     *
     * @throws UnsupportedOperationException if the clear operation
     *         is not supported by this collection
     */
    void clear();

    /**
     * Returns true if this collection contains the specified element.
     *
     * @param o element whose presence in this collection is to be tested
     * @return true if this collection contains the specified
     *         element
     * @throws ClassCastException if the type of the specified element
     *         is incompatible with this collection
     *
     * @throws NullPointerException if the specified element is null and this
     *         collection does not permit null elements
     */
    boolean contains(Object o);


    /**
     * Returns true if this collection contains all of the elements
     * in the specified collection.
     *
     * @param  c collection to be checked for containment in this collection
     * @return true if this collection contains all of the elements
     *         in the specified collection
     * @throws ClassCastException if the types of one or more elements
     *         in the specified collection are incompatible with this
     *         collection
     *         (<a href="#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified collection contains one
     *         or more null elements and this collection does not permit null
     *         elements
     *
     *         or if the specified collection is null.
     */
    boolean containsAll(Collection<?> c);


    /**
     * Compares the specified object with this collection for equality.
     *
     * @param o object to be compared for equality with this collection
     * @return true if the specified object is equal to this
     * collection
     */
    boolean equals(Object o);

    /**
     * Returns the hash code value for this collection.
     *
     * @return the hash code value for this collection
     *
     */
    int hashCode();

    /**
     * Returns true if this collection contains no elements.
     *
     * @return true if this collection contains no elements
     */
    boolean isEmpty();

    /**
     * Returns an iterator over the elements in this collection.
     * @return an Iterator over the elements in this collection
     */
    Iterator<E> iterator();

    /**
     * Returns a possibly parallel Stream with this collection as its source.
     * @return Stream
     */
    default Stream<E> parallelStream() {
        throw new UnsupportedOperationException("Invalid operation in Collection");
    }

    /**
     * Removes a single instance of the specified element from this
     * collection, if it is present (optional operation).
     *
     * @param o element to be removed from this collection, if present
     * @return true if an element was removed as a result of this call
     * @throws ClassCastException if the type of the specified element
     *         is incompatible with this collection
     *
     * @throws NullPointerException if the specified element is null and this
     *         collection does not permit null elements
     *
     * @throws UnsupportedOperationException if the remove operation
     *         is not supported by this collection
     */
    boolean remove(Object o);


    /**
     * Removes all of this collection's elements
     *
     * @param c collection containing elements to be removed from this collection
     * @return true if this collection changed as a result of the
     *         call
     * @throws UnsupportedOperationException if the removeAll method
     *         is not supported by this collection
     * @throws ClassCastException if the types of one or more elements
     *         in this collection are incompatible with the specified
     *         collection
     * @throws NullPointerException if this collection contains one or more
     *         null elements and the specified collection does not support
     *         null elements
     *         or if the specified collection is null
     */
    boolean removeAll(Collection<?> c);

    /**
     * Removes all of the elements of this collection that satisfy the given predicate.
     *
     * @param filter
     * @return
     */
    default boolean removeIf(Predicate<? super E> filter) {
        throw new UnsupportedOperationException("Invalid operation in Collection");
    }

    /**
     * Retains only the elements in this collection that are contained in the
     * specified collection
     *
     * @param c collection containing elements to be retained in this collection
     * @return true if this collection changed as a result of the call
     * @throws UnsupportedOperationException if the retainAll operation
     *         is not supported by this collection
     * @throws ClassCastException if the types of one or more elements
     *         in this collection are incompatible with the specified
     *         collection
     *
     * @throws NullPointerException if this collection contains one or more
     *         null elements and the specified collection does not permit null
     *         elements
     *
     *         or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */
    boolean retainAll(Collection<?> c);

    /**
     * Returns the number of elements in this collection.  If this collection
     * contains more than Integer.MAX_VALUE elements, returns
     * Integer.MAX_VALUE.
     *
     * @return the number of elements in this collection
     */
    int size();

    /**
     * Creates a Spliterator over the elements in this collection.
     * @return
     */
    default Spliterator<E> spliterator() {
        throw new UnsupportedOperationException("Invalid operation in Collection");
    }

    /**
     * Returns a sequential Stream with this collection as its source.
     * @return
     */
    default Stream<E> stream() {
        throw new UnsupportedOperationException("Invalid operation in Collection");
    }

    /**
     * Returns an array containing all of the elements in this collection.
     * @return an array containing all of the elements in this collection
     */
    Object[] toArray();

    /**
     * Returns an array containing all of the elements in this collection; the runtime type of the returned array is that of the specified array.
     * @param a the array
     * @param <T>
     * @return an array containing all of the elements in this collection
     */
    default  <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Invalid operation in Collection");
    }


}
