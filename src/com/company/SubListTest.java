package com.company;

import org.junit.Test;

import java.util.Iterator;
import java.util.ListIterator;

import static org.junit.Assert.*;

public class SubListTest<E>{

    //metodo utile
    public ListAdapter<E> getNewPopulatedList(){
        ListAdapter<E> lista = new ListAdapter<>();
        lista.add((E)"prova");
        lista.add((E)"abc");
        lista.add((E)"cde");

        return lista;
    }

    public Collection<E> getNewPopulatedCollection(){
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"nuovo1");
        coll.add((E)"nuovo2");
        coll.add((E)"nuovo3");

        return coll;
    }

    //metodo utile
    public void print(ListAdapter<E> lista){
        for(int i = 0 ; i < lista.size(); i++)
            System.out.println("Pos " + i + ": " + lista.get(i));
    }




    @Test
    public void testConstructor1(){  //check the sublist bounds are correctly initialised
        ListAdapter<E> lista = getNewPopulatedList();

        List<E> sottolista = lista.subList(0,3);    //the sublist is equals to the list

        assertEquals(0,((SubList<E>)sottolista).getFromIndex());
        assertEquals(3,((SubList<E>)sottolista).getToIndex());
    }

    @Test
    public void testConstructor2(){  //check the elements in the sublist are corresponding to the elements of the list
        ListAdapter<E> lista = getNewPopulatedList();

        List<E> sottolista = lista.subList(0,3);    //the sublist is equal to the list

        assertArrayEquals(lista.toArray(),sottolista.toArray());
    }

    @Test
    public void testConstructor3(){  //check the sublist is empty
        ListAdapter<E> lista = getNewPopulatedList();

        List<E> sottolista = lista.subList(0,0);    //the sublist is empty

        assertEquals(0,((SubList<E>)sottolista).getFromIndex());
        assertEquals(0,((SubList<E>)sottolista).getToIndex());
        assertEquals(0,((SubList<E>)sottolista).size());

    }


    @Test
    public void testGet1(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1); //sublist with only one element (the first)

        assertEquals(1,sottolista.size());
        assertEquals("prova",sottolista.get(0));
    }

    @Test
    public void testGet2(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3); //sublist with two elements

        assertEquals(2,sottolista.size());
        assertEquals("abc",sottolista.get(0));
        assertEquals("cde",sottolista.get(1));
    }

    @Test
    public void testSet1(){ //backing check
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.set(0,(E)"nuovo");

        assertEquals("nuovo",lista.get(0));
    }

    @Test
    public void testSet2(){ //check original list size remains untouched
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.set(0,(E)"nuovo");

        assertEquals(3,lista.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSet3(){ //check index out of bound is thrown (above the sublist range)
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.set(1,(E)"nuovo");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSet5(){ //check index out of bound is thrown (under the sublist range)
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.set(-1,(E)"nuovo");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSet4(){ //check index out of bound is thrown (negative index)
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.set(-1,(E)"nuovo");
    }

    @Test
    public void testAdd1(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.add((E) "nuovo");

        assertEquals("nuovo",sottolista.get(1));
        assertEquals(2,sottolista.size());
    }

    @Test
    public void testAdd2(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.add((E) "nuovo");

        assertEquals("nuovo",sottolista.get(2));
        assertEquals(3,sottolista.size());
    }


    @Test
    public void testAdd3(){ //check add updates the toIndex of the sublist
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.add((E) "nuovo");

        assertEquals(4,sottolista.getToIndex());
    }


    @Test
    public void testAddIndex1(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.add(0,(E) "nuovo");

        assertEquals("nuovo",sottolista.get(0));
        assertEquals("prova",sottolista.get(1));
        assertEquals(2,sottolista.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddIndex2(){    //check index out of bounds is thrown (index is above the sublist range)
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.add(1,(E) "nuovo");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddIndex3(){    //check index out of bounds is thrown (index is negative)
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.add(-1,(E) "nuovo");
    }


    @Test
    public void testAddIndex4(){
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.add(0,(E) "nuovo");

        assertEquals("nuovo",sottolista.get(0));
        assertEquals(3,sottolista.size());
    }


    public void testAddIndex5(){    //check add(index) updates toIndex in the sublist
        ListAdapter<E> lista = getNewPopulatedList();

        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.add(0,(E) "nuovo");

        assertEquals(4,sottolista.getToIndex());

    }



    @Test
    public void testIsEmpty1(){
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,0);

        assertTrue(sottolista.isEmpty());
    }

    @Test
    public void testIsEmpty2(){
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        assertFalse(sottolista.isEmpty());
    }

    @Test
    public void testIsEmpty3(){
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        sottolista.remove(0);
        assertTrue(sottolista.isEmpty());
    }



    @Test
    public void testRemoveIndex1(){  //check remove removes an element and return correct element
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,1);

        E result = sottolista.remove(0);

        assertEquals(0,sottolista.size());
        assertEquals(0,sottolista.getToIndex());
        assertEquals("prova",result);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndex2(){  //check remove does nothing if the sublist is empty
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,0);    //empty sublist

        E result = sottolista.remove(0);    //should throw IndexOutOfBOundException
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndex3(){  //check remove does nothing if the index is out of range (negative)
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        E result = sottolista.remove(-1);    //should throw IndexOutOfBOundException
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndex4(){  //check remove does nothing if the index is out of range (out of bounds)
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        E result = sottolista.remove(3);    //should throw IndexOutOfBOundException
    }


    @Test
    public void testRemoveIndex5(){  //check remove behaves correctly after an add
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        sottolista.add((E)"nuovo"); //nuovo is added and removed, sublist should have the same size again
        sottolista.remove(2);


        assertEquals(2,sottolista.size());
        assertEquals("cde",sottolista.get(1));
    }

    @Test
    public void testIndexOf1(){ //check indexOf behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        assertEquals(0,sottolista.indexOf("abc"));
        assertEquals(1,sottolista.indexOf("cde"));
    }

    @Test
    public void removeAllTest1(){   //check removeAll behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"abc");
        boolean result = sottolista.removeAll(coll);

        assertTrue(result);
        assertEquals(2,sottolista.size());
        assertFalse(sottolista.contains("abc"));
    }

    @Test
    public void removeAllTest2(){   //check removeAll behaves correctly (empty collection)
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        Collection<E> coll = new ListAdapter<>();   //empty collection
        boolean result = sottolista.removeAll(coll);

        assertFalse(result);
        assertEquals(3,sottolista.size());
    }

    @Test
    public void testIndexOf2(){ //check indexOf retruns -1 if an object isn not present in the sublist
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        assertEquals(-1,sottolista.indexOf("differente"));
    }

    @Test
    public void testRemove1(){ //check remove(object) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        boolean result = sottolista.remove("abc");   //removes the first element of the sublist

        assertEquals(1,sottolista.size());  //size diminishes
        assertTrue(result);
        assertEquals("cde",sottolista.get(0));  //abc has been removed, so the first element is now cde
    }

    @Test
    public void testRemove2(){ //check remove(object) returns false if the object isn't present in the sublist
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);

        boolean result = sottolista.remove("differente");   //removes the first element of the sublist

        assertEquals(2,sottolista.size());  //size remains the same
        assertFalse(result);
        assertEquals("abc",sottolista.get(0)); //abc has not been removed, so the first element is still abc
    }

    @Test
    public void testRemove3(){ //check remove(object) removes the FIRST occurrence of the object in the sublit
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);
        sottolista.add((E)"abc"); //a new occurrence of abc at index 2 (the first one is at index 0)

        boolean result = sottolista.remove("abc");   //removes the FIRST occurrence of abc (at index 0)

        assertEquals("cde",sottolista.get(0)); //the FIRST abc (index 0) has been removed, cde has been shifted to the left
        assertEquals("abc",sottolista.get(1)); //the second abc should exist (now on index 1)
    }


    @Test
    public void testToArray1(){  //check the array returned have the correct elements
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        Object[] arr = sottolista.toArray();

        assertEquals("prova",arr[0]);
        assertEquals("abc",arr[1]);
        assertEquals("cde",arr[2]);
    }

    @Test
    public void testToArray2(){  //check the array returned is empty if the sublist is empty
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,0);    //empty sublist
        Object[] arr = sottolista.toArray();    //should be empty

        assertEquals(0,arr.length);
    }

    @Test
    public void testClear1(){    //check clear behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        sottolista.clear();

        assertTrue(sottolista.isEmpty());
    }

    @Test
    public void testClear2(){    //check clear behaves correctly if the sublist is already empty
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,0);
        sottolista.clear();

        assertTrue(sottolista.isEmpty());
    }


    @Test
    public void testContains1(){    //check contains behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);

        assertTrue(sottolista.contains("abc"));
    }

    @Test
    public void testContains2(){    //check contains behaves correctly when element is not contained in the sublist
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);

        assertFalse(sottolista.contains("differente"));
    }

    @Test
    public void testAddAll1(){  //check addAll(Collection) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3

        Collection<E> coll = getNewPopulatedCollection();
        boolean result = sottolista.addAll(coll);

        assertTrue(result);
        assertEquals(6,sottolista.size());
    }

    @Test
    public void testAddAll2(){  //check addAll(Collection) behaves correctly if the collection is empty
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3

        Collection<E> coll = new ListAdapter<>();   //size of collection is 0
        boolean result = sottolista.addAll(coll);

        assertFalse(result);    //false = the sublist has not been modified
    }

    @Test
    public void testAddAllIndex1(){  //check addAll(index,Collection) behaves correctly (The new elements will appear in the list in the order that they are returned by the specified Collection's iterator.)
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3

        Collection<E> coll = getNewPopulatedCollection();  //size of collection is 3
        boolean result = sottolista.addAll(0,coll);  //starting from index 0


        Iterator<E> collIterator = coll.iterator();
        int i = 0;
        while(collIterator.hasNext()){
            assertEquals(sottolista.get(i),collIterator.next());
            i++;
        }

        assertEquals("prova",sottolista.get(3));
        assertEquals("abc",sottolista.get(4));
        assertEquals("cde",sottolista.get(5));

        assertTrue(result);
    }

    //testAddAllIndex when collection is empty would be redundant (watch testAddAll2)

    @Test
    public void testContainsAll1(){  //check containsAll return false if the sublist does not contain all elements of the collection
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3
        Collection<E> coll = getNewPopulatedCollection();

        assertFalse(sottolista.containsAll(coll));
    }

    @Test
    public void testContainsAll2(){  //check containsAll return true if the sublist contains all elements of the collection
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"abc");

        assertTrue(sottolista.containsAll(coll));
    }

    @Test
    public void testContainsAll3(){  //check containsAll return true if the sublist contains all elements of the collection
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"abc");
        coll.add((E)"prova");
        coll.add((E)"cde");

        assertTrue(sottolista.containsAll(coll));
    }

    @Test(expected = NullPointerException.class)
    public void testContainsAll4(){  //check containsAll throws NullPointerException if the collection is null
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3
        Collection<E> coll = null;

        sottolista.containsAll(coll);
    }

    @Test
    public void testContainsAll5(){  //check containsAll if the collection is empty
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //size of sublist is 3
        Collection<E> coll = new ListAdapter<>();

        assertTrue(sottolista.containsAll(coll));
    }

    @Test
    public void testLastIndexOf1(){ //check lastIndexOf behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);

        assertEquals(0,sottolista.lastIndexOf((E)"prova"));
    }

    @Test
    public void testLastIndexOf2(){ //check lastIndexOf behaves correctly (double occurrence "prova" in the sublist)
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        sottolista.add((E)"prova"); //second occurrence of "prova" at index 3

        assertEquals(3,sottolista.lastIndexOf((E)"prova"));
    }

    @Test
    public void testLastIndexOf3(){ //check lastIndexOf returns -1 if the object is not present in the sublist
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);

        assertEquals(-1,sottolista.lastIndexOf((E)"differente"));
    }

    @Test
    public void testEquals1(){  //check equals for two identical lists returns true
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);

        assertTrue(sottolista.equals(lista));
    }

    @Test
    public void testEquals2(){  //check equals for two identical lists returns true
        ListAdapter<E> lista = getNewPopulatedList();
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);
        sottolista.add((E)"differente");    //lista is backed by sottolista! both lists are still identical!

        assertTrue(sottolista.equals(lista));
    }

    @Test
    public void testEquals3(){  //check equals for two lists with different size returns false
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(1,3);    //sottolista's size is 2

        assertFalse(sottolista.equals(lista));
    }

    @Test
    public void testEquals4(){  //check equals for two lists with same size but different elements returns false
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        ListAdapter<E> lista2 = getNewPopulatedList();   //lista2's size is 3
        lista2.add((E)"nuovo");
        SubList<E> sottolista2 = (SubList<E>) lista2.subList(1,4);    //sottolista2's size is 3 but has different elements

        assertFalse(sottolista2.equals(sottolista));
    }

    @Test
    public void testRetainAll1(){   //check retainAll behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"prova");

        boolean result = sottolista.retainAll(coll);

        assertTrue(result);
        assertEquals(1,sottolista.size());
        assertTrue(sottolista.contains((E)"prova"));
    }


    @Test
    public void testRetainAll2(){   //check retainAll removes all elements if the collection is empty
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        Collection<E> coll = new ListAdapter<>(); //empty collection
        boolean result = sottolista.retainAll(coll);

        assertTrue(result);
        assertTrue(sottolista.isEmpty());
    }

    @Test
    public void testSubList1(){ //check subList behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        SubList<E> sottolista2 = (SubList<E>) sottolista.subList(1,3);   //sottolista's size is 2

        assertEquals(2,sottolista2.size());
        assertEquals("abc",sottolista2.get(0));
        assertEquals("cde",sottolista2.get(1));
    }

    @Test
    public void testSubList2(){ //check subList behaves correctly (empty sublist)
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        SubList<E> sottolista2 = (SubList<E>) sottolista.subList(0,0);   //sottolista's size is 0

        assertTrue(sottolista2.isEmpty());
    }

    @Test
    public void testSubList3(){ //check subList behaves correctly (backing is implemented)
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3
        SubList<E> sottolista2 = (SubList<E>) sottolista.subList(0,3);   //sottolista's size is 3

        sottolista2.set(0,(E)"nuovo");

        assertEquals("nuovo",sottolista.get(0));
    }

    @Test
    public void testIterator1(){    //check iterator behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 3

        Iterator<E> iterator = sottolista.iterator();
        int i = 0;

        while(iterator.hasNext()) {
            assertEquals(sottolista.get(i),iterator.next());
            i++;
        }
    }


    @Test
    public void testIterator2(){    //check iterator behaves correctly (empty sublist)
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,0);    //sottolista's size is 0

        Iterator<E> iterator = sottolista.iterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testListIterator1(){//check listiterator behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 0

        ListIterator<E> iterator = sottolista.listIterator();
        assertTrue(iterator.hasNext());
        assertFalse(iterator.hasPrevious());
    }

    @Test
    public void testListIteratorIndex1(){//check listiterator(index) behaves correctly
        ListAdapter<E> lista = getNewPopulatedList();   //lista's size is 3
        SubList<E> sottolista = (SubList<E>) lista.subList(0,3);    //sottolista's size is 0

        ListIterator<E> iterator = sottolista.listIterator(0);
        assertTrue(iterator.hasNext());
        assertFalse(iterator.hasPrevious());
    }

    //altri test su listIterator sono omessi poichè ridondanti con tutti quelli presenti in ListAdapterTest
}
