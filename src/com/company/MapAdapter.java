package com.company;

import java.util.*;


public class MapAdapter<K,V> implements Map<K,V>, Iterable<Map.Entry<K, V>> {
    Hashtable hashtable = new Hashtable();

    /**
     * Removes all of the mappings from this map (optional operation).
     * The map will be empty after this call returns.
     *
     * @throws UnsupportedOperationException if the {@code clear} operation
     *         is not supported by this map
     */
    @Override
    public void clear() {
        hashtable.clear();
    }

    /**
     * Returns {@code true} if this map contains a mapping for the specified
     * key.  More formally, returns {@code true} if and only if
     * this map contains a mapping for a key {@code k} such that
     * {@code Objects.equals(key, k)}.  (There can be
     * at most one such mapping.)
     *
     * @param key key whose presence in this map is to be tested
     * @return {@code true} if this map contains a mapping for the specified
     *         key
     * @throws ClassCastException if the key is of an inappropriate type for
     *         this map
     * @throws NullPointerException if the specified key is null and this map
     *         does not permit null keys
     */
    @Override
    public boolean containsKey(Object key) {
        return hashtable.containsKey(key);
    }

    /**
     * Returns true if this map maps one or more keys to the
     * specified value.  More formally, returns true if and only if
     * this map contains at least one mapping to a value true such that
     *  Objects.equals(value, v).  This operation
     * will probably require time linear in the map size for most
     * implementations of the  Map interface.
     *
     * @param value value whose presence in this map is to be tested
     * @return  if this map maps one or more keys to the
     *         specified value
     * @throws ClassCastException if the value is of an inappropriate type for
     *         this map
     *
     * @throws NullPointerException if the specified value is null and this
     *         map does not permit null values
     *
     */
    @Override
    public boolean containsValue(Object value) {
        return hashtable.contains(value);
    }

    /**
     * Return entry from Key and Value
     */

    static <K, V> MapAdapter.Entry<K, V> entry(K key, V value) {
        MapAdapter.Entry<K, V> entry = new MapAdapter.Entry<K, V>(key, value);
        entry.setImmutable();
        return entry;
    }
    /**
     * Returns a view of the mappings contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own remove operation, or through the
     * setValue operation on a map entry returned by the
     * iterator) the results of the iteration are undefined.  The set
     * supports element removal, which removes the corresponding
     * mapping from the map, via the Iterator.remove,
     * {@code Set.remove}, {@code removeAll}, {@code retainAll} and
     * {@code clear} operations.  It does not support the
     * {@code add} or {@code addAll} operations.
     *
     * @return a set view of the mappings contained in this map
     */
    @Override
    public Set<Map.Entry<K,V>> entrySet() {
        return new Set<>(){

            /**
             * add element
             * <strong>This operation is Unsupported</strong>
             *
             * @param kvEntry element to add
             * @return true if this collection changed as a result of the
             * call
             * @throws UnsupportedOperationException if the add operation
             *                                       is not supported by this collection
             * @throws ClassCastException            if the class of the specified element
             *                                       prevents it from being added to this collection
             * @throws NullPointerException          if the specified element is null and this
             *                                       collection does not permit null elements
             * @throws IllegalArgumentException      if some property of the element
             *                                       prevents it from being added to this collection
             * @throws IllegalStateException         if the element cannot be added at this
             *                                       time due to insertion restrictions
             */
            @Override
            public boolean add(Map.Entry<K, V> kvEntry) {
                throw new UnsupportedOperationException("Invalid operation in Set");
            }

            /**
             * Adds all of the elements in the specified collection to this collection
             * <strong>This operation is Unsupported</strong>
             *
             * @param c collection containing elements to be added to this collection
             * @return true if this collection changed as a result of the call
             * @throws UnsupportedOperationException if the addAll operation
             *                                       is not supported by this collection
             * @throws ClassCastException            if the class of an element of the specified
             *                                       collection prevents it from being added to this collection
             * @throws NullPointerException          if the specified collection contains a
             *                                       null element and this collection does not permit null elements,
             *                                       or if the specified collection is null
             * @throws IllegalArgumentException      if some property of an element of the
             *                                       specified collection prevents it from being added to this
             *                                       collection
             * @throws IllegalStateException         if not all the elements can be added at
             *                                       this time due to insertion restrictions
             *
             */
            @Override
            public boolean addAll(Collection<? extends Map.Entry<K, V>> c) {
                throw new UnsupportedOperationException("Invalid operation in Set");
            }

            /**
             * Removes all of the elements from this collection (optional operation).
             * The collection will be empty after this method returns.
             *
             * @throws UnsupportedOperationException if the clear operation
             *                                       is not supported by this collection
             */
            @Override
            public void clear() {
                MapAdapter.this.clear();
            }

            /**
             * Returns true if this collection contains the specified element.
             *
             * @param o element whose presence in this collection is to be tested
             * @return true if this collection contains the specified
             * element
             * @throws ClassCastException   if the type of the specified element
             *                              is incompatible with this collection
             * @throws NullPointerException if the specified element is null and this
             *                              collection does not permit null elements
             */
            @Override
            public boolean contains(Object o) {
                // l'oggetto che mi viene passato è una entry di map
                // Voglio sapere se è contenuta quella entry dentro alla mia mappa


                Map.Entry<K,V> e = (Map.Entry<K,V>) o;
                // fare il cast potrebbe ritornare eccezione

                Object key = e.getKey();
                Object value = e.getValue();

                if(MapAdapter.this.containsKey(key)){
                    if(MapAdapter.this.get(key) == value){
                        return true;
                    }else {
                        return false;
                    }
                }else{
                    return false;
                }

            }

            /**
             * Returns true if this collection contains all of the elements
             * in the specified collection.
             *
             * @param c collection to be checked for containment in this collection
             * @return true if this collection contains all of the elements
             * in the specified collection
             * @throws ClassCastException   if the types of one or more elements
             *                              in the specified collection are incompatible with this
             *                              collection
             *                              (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException if the specified collection contains one
             *                              or more null elements and this collection does not permit null
             *                              elements
             *                              <p>
             *                              or if the specified collection is null.
             */
            @Override
            public boolean containsAll(Collection<?> c) {

                Iterator<?> iterator = c.iterator();
                boolean contains = true;
                while (iterator.hasNext()){
                    if(!this.contains(iterator.next())){
                        // non contiene quella entry e quindi ritorno false
                        contains = false;
                        break;
                    }
                }

                return contains;
            }

            /**
             * Returns true if this collection contains no elements.
             *
             * @return true if this collection contains no elements
             */
            @Override
            public boolean isEmpty() {
                return MapAdapter.this.isEmpty();
            }

            /**
             * Returns an iterator over the elements in this collection.
             *
             * @return an Iterator over the elements in this collection
             */
            @Override
            public Iterator<Map.Entry<K, V>> iterator() {
                return  MapAdapter.this.iterator();
            }

            /**
             * Removes a single instance of the specified element from this
             * collection, if it is present (optional operation).
             *
             * @param o element to be removed from this collection, if present
             * @return true if an element was removed as a result of this call
             * @throws ClassCastException            if the type of the specified element
             *                                       is incompatible with this collection
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException          if the specified element is null and this
             *                                       collection does not permit null elements
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws UnsupportedOperationException if the remove operation
             *                                       is not supported by this collection
             */
            @Override
            public boolean remove(Object o) {
                MapAdapter.this.remove(o);
                return true;
            }

            /**
             * Removes all of this collection's elements
             *
             * @param c collection containing elements to be removed from this collection
             * @return true if this collection changed as a result of the
             * call
             * @throws UnsupportedOperationException if the removeAll method
             *                                       is not supported by this collection
             * @throws ClassCastException            if the types of one or more elements
             *                                       in this collection are incompatible with the specified
             *                                       collection
             * @throws NullPointerException          if this collection contains one or more
             *                                       null elements and the specified collection does not support
             *                                       null elements
             *                                       or if the specified collection is null
             */
            @Override
            public boolean removeAll(Collection<?> c) {


                Iterator<Map.Entry<K,V>> iter = (Iterator<Map.Entry<K, V>>) c.iterator();
                while (iter.hasNext()){

                    MapAdapter.this.remove(iter.next().getKey());
                }


                return true;
            }

            /**
             * Retains only the elements in this collection that are contained in the
             * specified collection
             *
             * @param c collection containing elements to be retained in this collection
             * @return true if this collection changed as a result of the call
             * @throws UnsupportedOperationException if the retainAll operation
             *                                       is not supported by this collection
             * @throws ClassCastException            if the types of one or more elements
             *                                       in this collection are incompatible with the specified
             *                                       collection
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException          if this collection contains one or more
             *                                       null elements and the specified collection does not permit null
             *                                       elements
             *                                       (<a href="#optional-restrictions">optional</a>),
             *                                       or if the specified collection is null
             */
            @Override
            public boolean retainAll(Collection<?> c) {

                int oldSize = size();
                Object[] collArr = c.toArray();
                Hashtable temphashtable = new Hashtable();

                if(collArr.length == 0)
                    clear();

                for(int i = 0 ; i<collArr.length; i++){ //cycles through the collection
                    //if an element of the collection is contained in the set, the element is added in to the temphashtable
                    if(contains(collArr[i]))
                        temphashtable.put(collArr[i],0);
                }

                hashtable = temphashtable;   //overwrite the current set with the temphashtable created

                return(size()!=oldSize);
            }

            /**
             * Returns the number of elements in this collection.  If this collection
             * contains more than Integer.MAX_VALUE elements, returns
             * Integer.MAX_VALUE.
             *
             * @return the number of elements in this collection
             */
            @Override
            public int size() {
                return MapAdapter.this.size();
            }

            /**
             * Returns an array containing all of the elements in this collection.
             *
             * @return an array containing all of the elements in this collection
             */
            @Override
            public Object[] toArray() {

                Object[] array = new Object[MapAdapter.this.size()];
                Iterator<Map.Entry<K, V>> iter = MapAdapter.this.iterator();
                int i = 0;

                while(iter.hasNext()){
                    array[i] = iter.next();
                    i++;
                }
                return array;
            }
        };
    };  //fine entrySet

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     *
     * <p>More formally, if this map contains a mapping from a key
     * {@code k} to a value {@code v} such that
     * {@code Objects.equals(key, k)},
     * then this method returns {@code v}; otherwise
     * it returns {@code null}.  (There can be at most one such mapping.)
     *
     * <p>If this map permits null values, then a return value of
     * {@code null} does not <i>necessarily</i> indicate that the map
     * contains no mapping for the key; it's also possible that the map
     * explicitly maps the key to null.  The containsKey
     * operation may be used to distinguish these two cases.
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or
     *          null if this map contains no mapping for the key
     * @throws ClassCastException if the key is of an inappropriate type for
     *         this map
     *
     * @throws NullPointerException if the specified key is null and this map
     *         does not permit null keys
     *
     */
    @Override
    public V get(Object key) {
        return (V) hashtable.get(key);
    }
    /**
     * Returns {@code true} if this map contains no key-value mappings.
     *
     * @return {@code true} if this map contains no key-value mappings
     */
    @Override
    public boolean isEmpty() {
        return hashtable.isEmpty();
    }

    /**
     * Returns a view of the keys contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own {@code remove} operation), the results of
     * the iteration are undefined.  The set supports element removal,
     * which removes the corresponding mapping from the map, via the
     * {@code Iterator.remove}, {@code Set.remove},
     * {@code removeAll}, {@code retainAll}, and {@code clear}
     * operations.  It does not support the {@code add} or {@code addAll}
     * operations.
     *
     * @return a set view of the keys contained in this map
     */
    @Override
    public Set<K> keySet() {
        return new SetAdapter<>(){
            {
                hashtable = MapAdapter.this.hashtable;
            }
            /**
             * UnsupportedOperationException
             */
            @Override
            public boolean add(K k) {
                throw new UnsupportedOperationException();
            }

            /**
             * UnsupportedOperationException
             * @param c
             */
            @Override
            public boolean addAll(Collection<? extends K> c) {
                throw new UnsupportedOperationException();
            }
        };

    }

    /**
     * Associates the specified value with the specified key in this map
     * (optional operation).  If the map previously contained a mapping for
     * the key, the old value is replaced by the specified value.  (A map
     * {@code m} is said to contain a mapping for a key {@code k} if and only
     * if (Object) m.containsKey(k) would return
     * {@code true}.)
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with {@code key}, or
     *         {@code null} if there was no mapping for {@code key}.
     *         (A {@code null} return can also indicate that the map
     *         previously associated {@code null} with {@code key},
     *         if the implementation supports {@code null} values.)
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map
     * @throws ClassCastException if the class of the specified key or value
     *         prevents it from being stored in this map
     * @throws NullPointerException if the specified key or value is null
     *         and this map does not permit null keys or values
     * @throws IllegalArgumentException if some property of the specified key
     *         or value prevents it from being stored in this map
     */
    @Override
    public V put(K key, V value) {
        return (V) hashtable.put(key,value) ;
    }

    /**
     * Copies all of the mappings from the specified map to this map
     * (optional operation).  The effect of this call is equivalent to that
     * of calling put(k, v)} on this map once
     * for each mapping from key {@code k} to value {@code v} in the
     * specified map.  The behavior of this operation is undefined if the
     * specified map is modified while the operation is in progress.
     *
     * @param m mappings to be stored in this map
     * @throws UnsupportedOperationException if the {@code putAll} operation
     *         is not supported by this map
     * @throws ClassCastException if the class of a key or value in the
     *         specified map prevents it from being stored in this map
     * @throws NullPointerException if the specified map is null, or if
     *         this map does not permit null keys or values, and the
     *         specified map contains null keys or values
     * @throws IllegalArgumentException if some property of a key or value in
     *         the specified map prevents it from being stored in this map
     */
    @Override
    public void putAll(Map<? extends K,? extends V> m) {

        Iterator<Map.Entry<K, V>> iter = m.iterator();
        while (iter.hasNext()){
            Map.Entry<K,V> elem = iter.next();
            put(elem.getKey(), elem.getValue());
        }



    }
    /**
     * Removes the mapping for a key from this map if it is present
     * (optional operation).   More formally, if this map contains a mapping
     * from key {@code k} to value {@code v} such that
     * {@code Objects.equals(key, k)}, that mapping
     * is removed.  (The map can contain at most one such mapping.)
     *
     * <p>Returns the value to which this map previously associated the key,
     * or {@code null} if the map contained no mapping for the key.
     *
     * <p>If this map permits null values, then a return value of
     * {@code null} does not <i>necessarily</i> indicate that the map
     * contained no mapping for the key; it's also possible that the map
     * explicitly mapped the key to {@code null}.
     *
     * <p>The map will not contain a mapping for the specified key once the
     * call returns.
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with {@code key}, or
     *         {@code null} if there was no mapping for {@code key}.
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this map
     * @throws ClassCastException if the key is of an inappropriate type for
     *         this map
     * @throws NullPointerException if the specified key is null and this
     *         map does not permit null keys
     */
    @Override
    public V remove(Object key) {

        return (V) hashtable.remove(key);
    }

    /**
     * Returns the number of key-value mappings in this map.  If the
     * map contains more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     *
     * @return the number of key-value mappings in this map
     */
    @Override
    public int size() {
        return hashtable.size();
    }

    /**
     * Returns a  view of the values contained in this map.
     * The collection is backed by the map, so changes to the map are
     * reflected in the collection, and vice-versa.  If the map is
     * modified while an iteration over the collection is in progress
     * (except through the iterator's own {@code remove} operation),
     * the results of the iteration are undefined.  The collection
     * supports element removal, which removes the corresponding
     * mapping from the map, via the {@code Iterator.remove},
     * {@code Collection.remove}, {@code removeAll},
     * {@code retainAll} and {@code clear} operations.  It does not
     * support the {@code add} or {@code addAll} operations.
     *
     * @return a collection view of the values contained in this map
     */
    @Override
    public Collection<V> values() {
        return new Collection<V>() {

            /**
             * add element
             * UnsupportedOperationException
             */
            @Override
            public boolean add(V v) {
                throw new UnsupportedOperationException();
            }

            /**
             * Adds all of the elements in the specified collection to this collection
             *
             * UnsupportedOperationException
             */
            @Override
            public boolean addAll(Collection<? extends V> c) {
                throw new UnsupportedOperationException();
            }

            /**
             * Removes all of the elements from this collection (optional operation).
             * The collection will be empty after this method returns.
             *
             * @throws UnsupportedOperationException if the clear operation
             *                                       is not supported by this collection
             */
            @Override
            public void clear() {
                MapAdapter.this.clear();
            }


            /**
             * Returns true if this collection contains the specified element.
             *
             * @param o element whose presence in this collection is to be tested
             * @return true if this collection contains the specified
             * element
             * @throws ClassCastException   if the type of the specified element
             *                              is incompatible with this collection
             * @throws NullPointerException if the specified element is null and this
             *                              collection does not permit null elements
             */
            @Override
            public boolean contains(Object o) {
                return MapAdapter.this.containsValue(o);
            }

            /**
             * Returns true if this collection contains all of the elements
             * in the specified collection.
             *
             * @param c collection to be checked for containment in this collection
             * @return true if this collection contains all of the elements
             * in the specified collection
             * @throws ClassCastException   if the types of one or more elements
             *                              in the specified collection are incompatible with this
             *                              collection
             *                              (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException if the specified collection contains one
             *                              or more null elements and this collection does not permit null
             *                              elements
             *                              <p>
             *                              or if the specified collection is null.
             */
            @Override
            public boolean containsAll(Collection<?> c) {
                Iterator<V> iter = (Iterator<V>) c.iterator();
                while (iter.hasNext()){
                    if(!contains(iter.next()))
                        return false;
                }
                return true;

            }

            /**
             * Returns true if this collection contains no elements.
             *
             * @return true if this collection contains no elements
             */
            @Override
            public boolean isEmpty() {
                return MapAdapter.this.isEmpty();
            }

            /**
             * Returns an iterator over the elements in this collection.
             *
             * @return an Iterator over the elements in this collection
             */
            @Override
            public Iterator<V> iterator() {
                return new Iterator<V>() {

                    Iterator<Map.Entry<K, V>> iterator = MapAdapter.this.iterator();

                    @Override
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    @Override
                    public V next() {
                        return iterator.next().getValue();
                    }

                    @Override
                    public void remove(){
                        iterator.remove();
                    }

                };
            }

            /**
             * Removes a single instance of the specified element from this
             * collection, if it is present (optional operation).
             *
             * @param o element to be removed from this collection, if present
             * @return true if an element was removed as a result of this call
             * @throws ClassCastException            if the type of the specified element
             *                                       is incompatible with this collection
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException          if the specified element is null and this
             *                                       collection does not permit null elements
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws UnsupportedOperationException if the remove operation
             *                                       is not supported by this collection
             */
            @Override
            public boolean remove(Object o) {
                Iterator<V> iter = iterator();
                while (iter.hasNext()){
                    if(o.equals(iter.next())){
                        iter.remove();
                        return true;
                    }
                }
                // if it has not been found the object to delete
                return false;


            }

            /**
             * Removes all of this collection's elements
             *
             * @param c collection containing elements to be removed from this collection
             * @return true if this collection changed as a result of the
             * call
             * @throws UnsupportedOperationException if the removeAll method
             *                                       is not supported by this collection
             * @throws ClassCastException            if the types of one or more elements
             *                                       in this collection are incompatible with the specified
             *                                       collection
             * @throws NullPointerException          if this collection contains one or more
             *                                       null elements and the specified collection does not support
             *                                       null elements
             *                                       or if the specified collection is null
             */
            @Override
            public boolean removeAll(Collection<?> c) {
                boolean retval = false;
                Iterator<V> iter = iterator();
                while (iter.hasNext()){
                    if(c.contains(iter.next())){
                        iter.remove();
                        retval = true;
                    }
                }
                return retval;
            }

            /**
             * Retains only the elements in this collection that are contained in the
             * specified collection
             *
             * @param c collection containing elements to be retained in this collection
             * @return true if this collection changed as a result of the call
             * @throws UnsupportedOperationException if the retainAll operation
             *                                       is not supported by this collection
             * @throws ClassCastException            if the types of one or more elements
             *                                       in this collection are incompatible with the specified
             *                                       collection
             *                                       (<a href="#optional-restrictions">optional</a>)
             * @throws NullPointerException          if this collection contains one or more
             *                                       null elements and the specified collection does not permit null
             *                                       elements
             *                                       (<a href="#optional-restrictions">optional</a>),
             *                                       or if the specified collection is null
             * @see #remove(Object)
             * @see #contains(Object)
             */
            @Override
            public boolean retainAll(Collection<?> c) {
                Iterator<V> iter = iterator();
                while (iter.hasNext()){
                    if (!c.contains(iter.next()))
                        iter.remove();
                }

                return true;
            }
            /**
             * Returns the number of elements in this collection.  If this collection
             * contains more than Integer.MAX_VALUE elements, returns
             * Integer.MAX_VALUE.
             *
             * @return the number of elements in this collection
             */
            @Override
            public int size() {
                return MapAdapter.this.size();
            }

            /**
             * Returns an array containing all of the elements in this collection.
             *
             * @return an array containing all of the elements in this collection
             */
            @Override
            public Object[] toArray() {

                Object[] array = new Object[MapAdapter.this.size()];
                Iterator<V> iter = iterator();
                int i = 0;
                while (iter.hasNext()){
                    array[i] = iter.next();
                    i++;
                }

                return array ;
            }
        };

    }//fine values()

    /**
     * method for return the iterator of the map
     * @return iterator of map
     */
    @Override
    public Iterator<Map.Entry<K, V>> iterator() {
        /**
         * rewritten the Iterator list going to redefine only the necessary methods
         */
        return  new ListIterator<>() {

            Enumeration keys = hashtable.keys();
            Enumeration values = hashtable.elements();

            MapAdapter.Entry<K, V> current;
            int i = 0;


            @Override
            public boolean hasNext() {
                return values.hasMoreElements();
            }

            @Override
            public Entry<K, V> next() {

                if(!hasNext()){
                    throw new NoSuchElementException();
                }
                i++;
                current = entry((K) keys.nextElement(), (V) values.nextElement());
                return current;
            }

            @Override
            public boolean hasPrevious() {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

            @Override
            public Entry<K, V> previous() {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

            @Override
            public int nextIndex() {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

            @Override
            public int previousIndex() {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

            @Override
            public void remove() {
                hashtable.remove(current.getKey());

            }

            @Override
            public void set(Map.Entry<K, V> kvEntry) {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

            @Override
            public void add(Map.Entry<K, V> kvEntry) {
                throw new UnsupportedOperationException("Invalid operation in ITERATOR OF MAP");
            }

        };
    }

    /**
     * A map entry (key-value pair).  The {@code Map.entrySet} method returns
     * a collection-view of the map, whose elements are of this class.  The
     * <i>only</i> way to obtain a reference to a map entry is from the
     * iterator of this collection-view.  These {@code Map.Entry} objects are
     * valid <i>only</i> for the duration of the iteration; more formally,
     * the behavior of a map entry is undefined if the backing map has been
     * modified after the entry was returned by the iterator, except through
     * the {@code setValue} operation on the map entry.
     *
     */
    public static class Entry <K,V> implements Map.Entry<K, V>  {
        private final K key;
        private V value;
        private boolean isImmutable = false;


        /**
         * Constructor method
         */

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public Entry(K key) {
            this.key = key;
        }




        /**
         * Ritorna la chiave della entry
         *
         * @return la chiave corrispondente della chiave
         * @throws IllegalStateException
         */
        public K getKey() {
            return key;
        }

        /**
         * Ritorna il valore corrispondente della entry
         *
         * @return valore corrispondente della entry
         * @throws IllegalStateException
         */
        public V getValue() {
            return value;
        }

        /**
         * Cambiare il valore corrispondente della entry specificata
         *
         * @param value
         * @return ritorna il vecchio valore corrispondente
         * @throws IllegalStateException
         * @throws ClassCastException
         * @throws IllegalArgumentException
         * @throws NullPointerException
         * @throws IllegalStateException
         */
        @Override
        public V setValue(V value) {
            if(isImmutable)
                throw new IllegalArgumentException("The Entry is immutable");

            V retval = this.value;
            this.value = value;
            return retval;

        }

        /**
         * Set the collection as immutable collection
         * Irreversible operation
         */
        public void setImmutable() {
            isImmutable = true;
        }

        /**
         * Get the collection as immutable collection
         * Irreversible operation
         */
        public boolean getImmutable() {
            return isImmutable;
        }

        /**
         * Comparare l'oggetto specifico con questa entry per comparare l'ugualianza
         *
         * @param o oggetto da comparare con la specifica entry.
         * @return true se gli oggetti sono uguali altrimenti false
         */
        @Override
        public boolean equals(Object o) {
            MapAdapter.Entry entry = (MapAdapter.Entry)o;
            if((key.equals(entry.getKey()))&&(value.equals(entry.getValue()))&&(isImmutable == entry.getImmutable())){
                return true;
            }
            return false;

        }

        /**
         * Ritorna il valore hash della entry specifica
         *
         * @return hash code della specifica entry.
         */
        public int hashCode() {
            return toString().hashCode();
        }

        /**
         * Return string of the current entry
         *
         * @return string of the entry
         */
        @Override
        public String toString() {
            return "Entry{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }




    }
}


