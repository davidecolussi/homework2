package com.company;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public class SetAdapter<E>  implements Set<E>{  //elements are stored as keys of the hashtable, the elements OF THE hashtable are not relevant (their value it's not specified)
                                                //null elements cannot be stored
    private Hashtable hashtable = new Hashtable();


    public int size(){
        return hashtable.size();
    }

    /**
     * Adds the specified element to this set if it is not already present.
     * @param element element to be added to this set.
     * @return true if the set did not already contain the specified element.
     */

    public boolean add(E element) {
        //check singularity of the element in this set
        if(!hashtable.containsKey(element)) {
            hashtable.put(element, 0);
            return true;
        }
        return false;
    }

    /**
     * Adds all of the elements in the specified collection to this set if they're not already present (optional operation). If the specified collection is also a set, the addAll operation effectively modifies this set so that its value is the union of the two sets. The behavior of this operation is unspecified if the specified collection is modified while the operation is in progress.
     * @param c collection containing elements to be added to this set
     * @return true if this set changed as a result of the call.
     * @throws NullPointerException - if the specified collection contains one or more null elements and this set does not support null elements, or if the specified collection is null.
     */
    
    public boolean addAll(Collection<? extends E> c) {
        Object[] collArr = c.toArray();
        int oldSize = size();

        for(int i = 0; i<collArr.length; i++){
            add((E)collArr[i]);
        }

        return(size()!=oldSize);
    }

    /**
     * Removes all of the elements from this set (optional operation). This set will be empty after this call returns (unless it throws an exception).
     */
    
    public void clear() {
        hashtable.clear();
    }

    /**
     * Returns true if this set contains the specified element. More formally, returns true if and only if this set contains an element e such that (o==null ? e==null : o.equals(e)).
     * @param o element whose presence in this collection is to be tested
     * @return true if this set contains the specified element.
     * @throws NullPointerException - if the specified element is null and this set does not support null elements (optional).
     */
    public boolean contains(Object o) {
        return hashtable.containsKey(o);
    }

    
    public boolean isEmpty() {
        return hashtable.isEmpty();
    }

    /**
     * Returns an iterator over the elements in this set. The elements are returned in no particular order (unless this set is an instance of some class that provides a guarantee).
     * @return an iterator over the elements in this set.
     */

    @Override
    public Iterator<E> iterator(){
        return new Iterator<E>() {
            Enumeration<E> enumeration = hashtable.keys();

            @Override
            public boolean hasNext() {
                return enumeration.hasMoreElements();
            }

            @Override
            public E next() {
                return enumeration.nextElement();
            }
        };
    }

    /**
     * Removes the specified element from this set if it is present. (optional operation)
     * @param o element to be removed from this collection, if present
     * @return true if the set contained the specified element.
     */
    
    public boolean remove(Object o) {
        if(hashtable.remove(o) == null)
            return false;
        return true;
    }


    /**
     * Returns an array containing all of the elements in this set. Obeys the general contract of the Collection.toArray method.
     * @return an array containing all of the elements in this set.
     */
    public Object[] toArray() {
        Object[] arr = new Object[size()];
        Enumeration<E> enumeration = hashtable.keys();
        int i = 0;

        while(enumeration.hasMoreElements()) {
            arr[i] = enumeration.nextElement();
            i++;
        }

        return arr;
    }

    /**
     * Retains only the elements in this set that are contained in the specified collection (optional operation).
     * In other words, removes from this set all of its elements that are not contained in the specified collection.
     * If the specified collection is also a set, this operation effectively modifies this set so that
     * its value is the intersection of the two sets.
     * @param c collection containing elements to be retained in this collection
     * @return true if this collection changed as a result of the call.
     */
    
    public boolean retainAll(Collection c) {
        int oldSize = size();
        Object[] collArr = c.toArray();
        Hashtable temphashtable = new Hashtable();

        if(collArr.length == 0)
            clear();

        for(int i = 0 ; i<collArr.length; i++){ //cycles through the collection
            //if an element of the collection is contained in the set, the element is added in to the temphashtable
            if(contains(collArr[i]))
                temphashtable.put(collArr[i],0);
        }

        hashtable = temphashtable;   //overwrite the current set with the temphashtable created

        return(size()!=oldSize);
    }

    /**
     * Removes from this set all of its elements that are contained in the specified collection (optional operation).
     * @param c collection containing elements to be removed from this collection
     * @return true if this set changed as a result of the call.
     * @throws NullPointerException - if the specified collection is null.
     */
    
    public boolean removeAll(Collection c) {
        if(c == null)
            throw new NullPointerException();

        Object[] collArr = c.toArray();
        int oldSize = size();

        for(int i = 0 ; i<collArr.length; i++)
            remove(collArr[i]);

        return(size()!=oldSize);
    }


    /**
     * Returns true if this collection contains all of the elements in the specified collection.
     * @param  c collection to be checked for containment in this collection
     * @return true if this collection contains all of the elements in the specified collection
     * @throws NullPointerException - if the specified collection is null.
     */

    public boolean containsAll(Collection c) {
        if(c == null)
            throw new NullPointerException();

        Object[] collArr = c.toArray();

        for(int i = 0 ; i < collArr.length; i++){
            if(!contains(collArr[i]))
                return false;
        }

        return true;
    }

    /**
     * Compares the specified object with this set for equality. Returns true if the specified object is also a set, the two sets have the same size, and every member of the specified set is contained in this set.
     * @param o object to be compared for equality with this collection
     * @return
     */
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Set))
            return false;
        if(((Set) o).size() != size())
            return false;
        if(containsAll((Collection)o))  //this and o have same size, check for equality of all the elements
           return true;

        return false;
    }

    /**
     * Returns the hash code value for this set. The hash code of a set is defined to be
     * the sum of the hash codes of the elements in the set, where the hashcode of a null element
     * is defined to be zero.
     * This ensures that s1.equals(s2) implies that s1.hashCode()==s2.hashCode() for any two sets s1 and s2,
     * as required by the general contract of the Object.hashCode method.
     * @return the hash code value for this set.
     */
    @Override
    public int hashCode(){
        int hashcode = 0;
        Enumeration<E> enumeration = hashtable.keys();

        while(enumeration.hasMoreElements()){
            hashcode += enumeration.nextElement().hashCode();
        }

        return hashcode;
    }

}
