package com.company;

import org.junit.runner.*;
import org.junit.runner.notification.Failure;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Result result = JUnitCore.runClasses(ListAdapterTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println("Il risultato del test sull'unità ListAdapter è: " + result.wasSuccessful());

        Result result2 = JUnitCore.runClasses(SubListTest.class);
        for (Failure failure : result2.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println("Il risultato del test sull'unità SubList è: " + result2.wasSuccessful());

        Result result3 = JUnitCore.runClasses(SetAdapterTest.class);
        for (Failure failure : result3.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println("Il risultato del test sull'unità SetAdapter è: " + result3.wasSuccessful());

        Result result4 = JUnitCore.runClasses(MapAdapterTest.class);
        for (Failure failure : result4.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println("Il risultato del test sull'unità MapAdapter è: " + result4.wasSuccessful());







    }
}
