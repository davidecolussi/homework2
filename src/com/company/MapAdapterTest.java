package com.company;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.Vector;

import static org.junit.Assert.assertEquals;

public class MapAdapterTest<K,V> {


    /**
     * Metodo che ritorna una mappa popolata per eseguire la suite di test
     */

    public Map<K,V> getMappaPopolata(){

        Map<K,V> mappa = new MapAdapter<K,V>();

        Integer k = new Integer(1);
        String v = "ciao1";
        mappa.put((K)k, (V) v);

        k = new Integer(2);
        v = "ciao2";
        mappa.put((K)k, (V) v);

        k = new Integer(3);
        v = "ciao3";
        mappa.put((K)k, (V) v);

        return mappa;
    }



    /**
     * Test per verificare che vengano eliminati sutti gli elementi presenti nella mappa
     */
    @Test
    public void testClear1(){
        Map<K,V> mappa = getMappaPopolata();

        mappa.clear();


        assertEquals(0,mappa.size());
    }

    /**
     * Test per verificare che sia presente una data chiave all,interno della mappa
     * pre-condizione = sia presente una entry con la chiave che vogliamo cercare
     */

    @Test
    public void testContainsKey1(){

        Map<K,V> mappa = getMappaPopolata();
        Integer key = new Integer(1);

        assertEquals(true,mappa.containsKey(key));


    }

    /**
     * Test per verificare che sia presente una data chiave all,interno della mappa
     * pre-condizione = NON sia presente una entry con la chiave che vogliamo cercare
     */

    @Test
    public void testContainsKey2(){

        Map<K,V> mappa = getMappaPopolata();
        Integer key = new Integer(8);

        assertEquals(false,mappa.containsKey(key));


    }

    /**
     * Test per verificare che sia presente una entry con un dato valore all'interno della mappa
     * pre-condizione = sia presente una entry con il valore che vogliamo cercare
     */
    @Test
    public void testContainsValue1(){

        Map<K,V> mappa = getMappaPopolata();
        String v = "ciao1";

        assertEquals(true,mappa.containsValue(v));
    }

    /**
     * Test per verificare che sia presente una entry con un dato valore all'interno della mappa
     * pre-condizione = NON sia presente una entry con il valore che vogliamo cercare
     */
    @Test
    public void testContainsValue2(){

        Map<K,V> mappa = getMappaPopolata();
        String v = "Tsdfs"; // valore non contenuto nella mappa

        assertEquals(false,mappa.containsValue(v));
    }

    /**
     *Test per verificare che mi venga ritornata una Map.entry che sia immutabile contente la chiave e valore specificati
     *Confronto 2 Entry uguali
     */

    @Test
    public void testEntry1(){

        Map<K,V> mappa = new MapAdapter<>();

        Integer k = new Integer(1);
        String v = "ciao"; // valore non contenuto nella mappa
        MapAdapter.Entry<K,V> entryProva = MapAdapter.entry((K) k,(V) v);
        MapAdapter.Entry<K,V> entryTest = new MapAdapter.Entry<K,V>((K) k,(V)v);
        entryTest.setImmutable();


        assertEquals(true,entryTest.equals(entryProva));
    }

    /**
     *Test per verificare che mi venga ritornata una Map.entry che sia immutabile contente la chiave e valore specificati
     *Confronto 2 Entry diverse
     */

    @Test
    public void testEntry2(){

        Map<K,V> mappa = new MapAdapter<>();

        Integer k = new Integer(1);
        String v = "ciao"; // valore non contenuto nella mappa
        MapAdapter.Entry<K,V> entryProva = MapAdapter.entry((K) k,(V) v);
        MapAdapter.Entry<K,V> entryTest = new MapAdapter.Entry<K,V>((K) k,(V)v); // rimane mutabile come chiave


        assertEquals(false, entryTest.equals(entryProva));
    }


    /**
     * Test the Set is backed by the Map
     */

    /**
     * Test per verificare che il set.clear() modifichi anche la mappa originale
     */

    @Test
    public void testEntrySet_clear(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        assertEquals(3, set.size());
        assertEquals(3, mappa.size());
        set.clear();
        assertEquals(0, set.size());
        assertEquals(0, mappa.size());
    }

    /**
     * Test per verificare che funzionai il metodo contain di set restituito dal metodo entrySet di MapAdapter
     * Mi aspetto che ritorni eccezione perchè faccio la ricerca su set di un oggetto che non è entry ben si una chiave
     */
    @Test(expected = ClassCastException.class)
    public void testEntrySet_contains1(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();
        Integer key = new Integer(1);

        assertEquals( true , mappa.containsKey(key));
        assertEquals( false , set.contains(key));

    }

    /**
     * Test per verificare che funzionai il metodo contain di set restituito dal metodo entrySet di MapAdapter
     * Mi aspetto che la ricerca vada a buon fine
     */
    @Test()
    public void testEntrySet_contains2(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        Integer key = new Integer(1);
        String value = "ciao1";

        MapAdapter.Entry<K,V> e = new MapAdapter.Entry<K,V>((K) key, (V) value);

        assertEquals( true , set.contains(e));

    }

    /**
     *Test del metodo isEmptyt sul set restituto dal metodo entrySet di MapAdapter
     */
    @Test
    public void testEntrySet_isEmpty(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();
        mappa.clear();

        assertEquals(true, set.isEmpty());
    }


    /**
     * Test che verifica che sia contenuto un collection di entry all'interno della set che mi viene restituta dal metodo entrySet di MapAdapter
     */
    @Test
    public void testEntrySet_containsAll(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        Integer key1 = new Integer(1);
        String value1 = "ciao1";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(2);
        String value2 = "ciao2";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);
        subSet.add(e2);


        assertEquals( true , set.containsAll(subSet));
    }

    /**
     * Test iterator su set ritornato da entrySet di map
     */

    @Test
    public void testEntrySet_iterator(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        Iterator<Map.Entry<K,V>> iteratorSet = set.iterator();


        Integer key1 = new Integer(3);
        String value1 = "ciao3";
        Map.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);



        assertEquals( true , iteratorSet.hasNext());
        if(iteratorSet.hasNext()) {
            Map.Entry<K,V> aux = (MapAdapter.Entry<K, V>) iteratorSet.next();

            assertEquals(e1.getKey(), aux.getKey());
            assertEquals(e1.getValue(), aux.getValue());

        }

    }


    /**
     * Test metodo remove di set ritornato dal metodo entrySet di mappa
     */
    @Test
    public void testEntrySet_remove(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        Integer key1 = new Integer(3);


        set.remove(key1);
        assertEquals( 2, set.size());
        assertEquals( 2, mappa.size());

    }


    /**
     * Test metodo remove di set restituito dal metodo entrySet di MapAdapter
     * Eliminare dalla se (e di conseguenza dalla mappa) tutti gli elemnti contenuti nella collection passata alla funzione
     */
    @Test
    public void testEntrySet_removeAll(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();
        Iterator<Map.Entry<K,V>> iteratorSet = set.iterator();

        Integer key1 = new Integer(1);
        String value1 = "ciao1";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(2);
        String value2 = "ciao2";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);
        subSet.add(e2);

        set.removeAll(subSet);

        Map.Entry<K,V> aux = iteratorSet.next();

        assertEquals(3, aux.getKey() );
        assertEquals(1, mappa.size());

    }


    /**
     * Test del metodo retainAll del set passato a
     */


    @Test
    public void testEntrySet_reatainAll1(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();


        Integer key1 = new Integer(1);
        String value1 = "ciao1";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(2);
        String value2 = "ciao2";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);
        subSet.add(e2);

        set.retainAll(subSet);

        assertEquals(2, set.size());

    }

    @Test
    public void testEntrySet_reatainAll2(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();


        Integer key1 = new Integer(1);
        String value1 = "ciao1";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(2);
        String value2 = "ciao2";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);

        set.retainAll(subSet);

        assertEquals(1, set.size());

    }

    @Test
    public void testEntrySet_reatainAll3(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();


        Integer key1 = new Integer(1);
        String value1 = "ciao1";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(3);
        String value2 = "ciao2";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);

        set.retainAll(subSet);

        assertEquals(1, set.size());

    }

    @Test
    public void testEntrySet_reatainAll4(){
        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();


        Integer key1 = new Integer(1);
        String value1 = "ciao";

        MapAdapter.Entry<K,V> e1 = new MapAdapter.Entry<K,V>((K) key1, (V) value1);

        Integer key2 = new Integer(3);
        String value2 = "ciao";

        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        Collection<MapAdapter.Entry<K,V>> subSet = new ListAdapter<MapAdapter.Entry<K,V>>();
        subSet.add(e1);

        set.retainAll(subSet);

        assertEquals(0, set.size());

    }


    /**
     * Test metodo size di set che viene passato dal metodo entrySet
     */
    @Test
    public void testEntrySet_size(){

        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();

        assertEquals(3, set.size());
    }

    /**
     * Test metodo toArray di set ritornato dal metodo entrySet di MapAdapter
     */

    @Test
    public void testEntrySet_toArray(){

        Map<K,V> mappa = getMappaPopolata();
        Set<Map.Entry<K, V>> set = mappa.entrySet();
        Object[] array = set.toArray();

        Integer key2 = new Integer(2);
        String value2 = "ciao2";
        MapAdapter.Entry<K,V> e2 = new MapAdapter.Entry<K,V>((K) key2, (V) value2);

        assertEquals("Entry{key=2, value=ciao2}", array[1].toString());
    }

    /**
     * Test metodo get della classe MapAdapter
     * Si suppone che la chaive passata sia associata ad una entry contenuta in mappa
     */
    @Test
    public void testGet1(){
        Map<K,V> mappa = getMappaPopolata();

        Integer key = new Integer(1);
        assertEquals("ciao1", mappa.get(key));
    }

    /**
     * Test metodo get della classe MapAdapter
     * Si suppone che la chaive passata NON sia associata ad una entry contenuta in mappa
     * e quindi non viene trovata la entry con la relativa chiave passata
     */
    @Test
    public void testGet2(){
        Map<K,V> mappa = getMappaPopolata();

        Integer key = new Integer(5);
        assertEquals(null, mappa.get(key));
    }

    /**
     * Test metodo isEmpty che mi restituisce true se la mappa è vuota altrimenti false
     */
    @Test
    public void testIsEmpty(){
        Map<K,V> mappa = getMappaPopolata();
        assertEquals(false, mappa.isEmpty());
    }

    /**
     * Test metodo keySet
     */
    //METODO NON FUNZIONANTE
    @Test
    public void testKeySet(){
        Map<K,V> mappa = getMappaPopolata();

        assertEquals(true, true);
    }

    /**
     * Test sul metodo put della classe MapAdapte
     * Ci si aspetta che venga inserito un nuovo elemento
     */
    @Test
    public void testPut(){
        Map<K,V> mappa = getMappaPopolata();
        assertEquals(3, mappa.size());

        Integer key = new Integer(5);
        String value = "prova";

        mappa.put((K) key,(V) value);
        assertEquals(4, mappa.size());
    }

    /**
     * Test sul metodo putAll della classe MapAdapte
     * Ci si aspetta che vengano inseriti tutti gli elementi della collection passata alla funzione
     */
    @Test
    public void testPutAll(){
        Map<K,V> mappa = getMappaPopolata();


        Integer key1 = new Integer(4);
        String value1 = "ciao4";


        Integer key2 = new Integer(5);
        String value2 = "ciao5";


        Map<K,V> subMap = new MapAdapter<K,V>();
        subMap.put((K) key1, (V) value1);
        subMap.put((K) key2, (V) value2);

        assertEquals(3, mappa.size());
        mappa.putAll(subMap);

        assertEquals(5, mappa.size());

    }

    /**
     * Test metodo remove di MapAdapter
     */
    @Test
    public void testRemove(){

        Map<K,V> mappa = getMappaPopolata();
        assertEquals(3, mappa.size());
        Integer key1 = new Integer(4);
        mappa.remove(key1);
        assertEquals(3, mappa.size());

        Integer key2 = new Integer(1);
        mappa.remove(key2);

        assertEquals(2, mappa.size());

        Integer key3 = new Integer(2);
        mappa.remove(key3);
        Integer key4 = new Integer(3);
        mappa.remove(key4);
        Integer key5 = new Integer(4);
        mappa.remove(key5);





    }

    /**
     * Test metodo size di MapAdapter
     */

    @Test
    public void testSize(){
        Map<K,V> mappa = getMappaPopolata();
        assertEquals(3, mappa.size());
    }


    // TEST VALUES COLLECTION ******************************************************************************************************

    /**
     * Test clear collection ritornato in values di MapAdapter
     */
    @Test
    public void testValues_clear1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        // check dimension
        assertEquals(3, values.size());

        values.clear();
        assertEquals(0, mappa.size());

    }

    @Test
    public void testValues_clear2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        // check dimension
        assertEquals(3, values.size());

        mappa.clear();
        assertEquals(0, mappa.size());
        assertEquals(0, values.size());
    }

    @Test
    public void testValues_clear3(){
        Map<K,V> mappa = new MapAdapter<K,V> ();
        Collection<V> values = mappa.values();

        // check dimension
        assertEquals(0, values.size());

        mappa.clear();
        assertEquals(0, mappa.size());
        assertEquals(0, values.size());
    }

    /**
     * Test contain di collection ritornata dal metodo values di MapAdapter
     */

    @Test
    public void testValues_contains(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        assertEquals(true, values.contains(v1));

        String v2 = "pippo";
        assertEquals(false, values.contains(v2));

    }

    /**
     * Test containAll di collection ritornata dal metodo values di MapAdapter
     */
    @Test
    public void testValues_containsAll1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "ciao2";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);

        assertEquals(true, values.containsAll(subCollection));

    }

    @Test
    public void testValues_containsAll2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "pippo";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);

        assertEquals(false, values.containsAll(subCollection));

    }

    /**
     * Test metodo isEmpty di collection ritornata dal metodo values di MapAdapter
     */
    @Test
    public void testValues_isEmpty(){
        Map<K,V> mappa = new MapAdapter<K,V> ();
        Collection<V> values = mappa.values();

        assertEquals(true, values.isEmpty());
        Integer key2 = new Integer(2);
        String value2 = "ciao2";
        mappa.put((K) key2, (V) value2);
        assertEquals(false, values.isEmpty());
    }

    /**
     * Test metodo iterator di collection ritornata dal metodo values di MapAdapter
     */
    @Test
    public void testValues_Iterator1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();
        Iterator<V> iter = values.iterator();
        if (iter.hasNext()){
            assertEquals("ciao3" , iter.next());
        }


    }

    @Test(expected = java.util.NoSuchElementException.class)
    public void testValues_Iterator2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();
        Iterator<V> iter = values.iterator();
        while (iter.hasNext()){
            iter.next();

        }
        assertEquals(true, iter.next());

    }

    /**
     * Test metodo remove di collection ritornata dal metodo values di MapAdapter
     */
    //Mi aspetto che non vengano eliminati nessun elemnto perchè non sto passando un valore presente nella collection
    @Test
    public void testValues_remove1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        assertEquals(3, mappa.size());

        Integer key2 = new Integer(2);
        values.remove(key2);
        assertEquals(3, mappa.size());
        assertEquals(3, values.size());
    }

    //Mi aspetto che venga eliminato il valore con il valore passato
    @Test
    public void testValues_remove2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        assertEquals(3, mappa.size());

        String v1 = "ciao1";
        values.remove(v1);
        assertEquals(2, mappa.size());
        assertEquals(2, values.size());
    }
    /**
     * Test metodo removeAll di collection ritornata dal metodo values di MapAdapter
     */
    @Test
    public void testValues_removeAll1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "ciao2";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);
        values.removeAll(subCollection);

        assertEquals(1, values.size());
    }


    @Test
    public void testValues_removeAll2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "pippo";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);
        values.removeAll(subCollection);

        assertEquals(2, values.size());
    }
    /**
     * Test reatinAll
     */
    @Test
    public void testValues_retainAll1(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "ciao2";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);
        values.retainAll(subCollection);

        assertEquals(2, values.size());
    }
    @Test
    public void testValues_retainAll2(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        String v1 = "ciao1";
        String v2 = "pippo";

        Collection<V> subCollection = (Collection<V>) new ListAdapter<K>();
        subCollection.add((V) v1);
        subCollection.add((V) v2);
        values.retainAll(subCollection);

        assertEquals(1, values.size());
    }

    /**
     * Test metodo size
     */
    @Test
    public void testValues_size(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        assertEquals(3, values.size());
    }

    /**
     * Test toArray
     */
    @Test
    public void testValues_toArray(){
        Map<K,V> mappa = getMappaPopolata();
        Collection<V> values = mappa.values();

        Object[] array = values.toArray();
        for(int i = 0 ; i < array.length ; i++){
            if(i == 0)
                assertEquals("ciao3", array[i]);
        }

    }


}
