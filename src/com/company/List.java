package com.company;

import java.util.*;
import java.util.function.UnaryOperator;

public interface List<E> extends Collection<E>{      //interfaccia target

    /**
     * Return the element at the specified position in this list
     * @param index the specified position
     * @return the object at the specified index
     * @throws IndexOutOfBoundsException if an invalid index was given
     */
    E get(int index);

    /**
     * Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
     * @param o the object to search in the list
     * @return the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
     */

    int indexOf(Object o);

    /**
     * Returns the index of the last occurrence of the specified object in this vector.
     * @param o the object to search in the list
     * @return the index of the last occurrence of the specified object in this vector; returns -1 if the object is not found.
     */

     int lastIndexOf(Object o);

    /**
     * Returns a list iterator over the elements in this list (in proper sequence).
     * @return the list iterator over the elements in this list
     */

    ListIterator<E> listIterator();

    /**
     * Returns a list iterator over the elements in this list (in proper sequence), starting at the specified position in the list.
     * @param index the starting index
     * @return the list iterator over the elements in this list (in proper sequence), starting at the specified position in the list.
     */

    ListIterator<E> listIterator(int index);

    /**
     * removes the element at the given index
     * @param index the index which object associated has to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException - if the index is out of range
     */

    E remove(int index);

    /**
     * Replaces the element at the specified position in this list with the specified element (optional operation).
     * @param index the index of the list
     * @param element the new element
     * @return the element previously at the specified position
     */

    E set(int index, E element);

    /**
     * Returns a immutable list containing zero elements
     * @param <E>
     * @return
     */
    static <E> List<E> of(){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing one element
     * @param e1
     * @param <E>
     * @return
     */
    static <E> List<E> of(E e1){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing an arbitrary number of elements
     * @param elements
     * @param <E>
     * @return
     */
    static <E> List<E> of(E... elements){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing two elements
     * @param e1
     * @param e2
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing three elements
     * @param e1
     * @param e2
     * @param e3
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2, E e3){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing four elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param <E>
     * @return
     */
    static <E> List<E> of(E e1, E e2, E e3, E e4){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing five elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param <E>
     * @return
     */
    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing six elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param e6
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5, E e6){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing seven elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param e6
     * @param e7
     * @param <E>
     * @return
     */
    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing eight elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param e6
     * @param e7
     * @param e8
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing nine elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param e6
     * @param e7
     * @param e8
     * @param e9
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a immutable list containing ten elements
     * @param e1
     * @param e2
     * @param e3
     * @param e4
     * @param e5
     * @param e6
     * @param e7
     * @param e8
     * @param e9
     * @param e10
     * @param <E>
     * @return
     */

    static <E> List<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10){
        throw new UnsupportedOperationException();
    };

    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @param fromIndex starting index (inclusive)
     * @param toIndex finishing index (exclusive)
     * @return a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @throws IndexOutOfBoundsException - for an illegal endpoint index value
     * @see SubList
     */
    List<E> subList(int fromIndex, int toIndex);

    /**
     * Replaces each element of this list with the result of applying the operator to that element
     * @param operator
     */
    default void replaceAll(UnaryOperator<E> operator){
        throw new UnsupportedOperationException();
    }

    /**
     * Sorts this list according to the order induced by the specified Comparator.
     * @param c the comparator
     */
    default void sort(Comparator<? super E> c){
    }



}
