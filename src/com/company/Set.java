package com.company;

public interface Set<E> extends Collection<E>{

    static <E> Set<E> of() {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E... elements) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5, E e6) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }

    static <E> Set<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10) {
        throw new UnsupportedOperationException("Invalid operation in Set");
    }



}
