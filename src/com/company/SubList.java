package com.company;

import org.junit.Test;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Vector;

public class SubList<E> extends ListAdapter<E>{
    private ListAdapter<E> parent;
    int fromIndex;
    int toIndex;
    int size;

    /**
     * construct a subList of a given list
     * @param p the parent list
     * @param fi from index (inclusive)
     * @param ti to index (exclusive)
     */
    public SubList(ListAdapter<E> p, int fi, int ti){
        parent = p;
        fromIndex = fi;
        toIndex = ti;
        size = toIndex - fromIndex;
    }

    /**
     * returns fi
     * @return fi, the "from index" (inclusive)
     */
    public int getFromIndex() {
        return fromIndex;
    }

    /**
     * returns ti
     * @return fi, the "to index" (exclusive)
     */
    public int getToIndex() {
        return toIndex;
    }


    /**
     * @param index
     * @return the object at the specified index. The index is relative to the sublist
     * @throws IndexOutOfBoundsException if an invalid index was given
     */
    @Override
    public E get(int index){
        return parent.get(fromIndex + index);
    }

    /**
     * Replaces the element at the specified position in this sublist with the specified element (optional operation). The index is relative to the sublist
     * @param index
     * @param element
     * @return the old element in the list at the index position
     * @throws IndexOutOfBoundsException if the index is out of the sublist range
     */

    @Override
    public E set(int index, E element){
        if(fromIndex + index >= toIndex || index < 0){
            IndexOutOfBoundsException e = new IndexOutOfBoundsException("Index out of the range of the sublist");
            throw e;
        }
        E oldElement = get(index);

        parent.set(fromIndex + index, element);

        return oldElement;
    }

    /**
     * Returns the actual size of the sublist
     * @return the size of the sublist (toIndex - fromIndex)
     */
    @Override
    public int size(){
        return size;
    }


    /**
     * Appends the specified element to the end of this sublist (optional operation).
     * @return true if the list has been modified by this method
     */
    @Override
    public boolean add(E elem){
        parent.add(toIndex,elem);
        size++;
        toIndex++;

        return true;   //the list has been modified by this method
    }

    /**
     * Inserts the specified element at the specified position in this sublist (optional operation).
     *
     */
    @Override
    public void add(int index, E elem){
        if(fromIndex + index >= toIndex || index < 0){
            IndexOutOfBoundsException e = new IndexOutOfBoundsException("Index out of the range of the sublist");
            throw e;
        }

        parent.add(fromIndex + index,elem);
        size++;
        toIndex++;
    }

    /**
     * Returns true if the sublist is empty
     * @return true if the sublist is empty
     */
    @Override
    public boolean isEmpty(){
        return(size() == 0);
    }

    /**
     * removes the element at the given index. Index is relative to the sublist
     * @param index
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException - if the index is out of range
     */

    @Override
    public E remove(int index){
        E result = get(index);

        if(index >= size() || index < 0){
            IndexOutOfBoundsException e = new IndexOutOfBoundsException("Index out of the range of the sublist");
            throw e;
        }

        parent.remove(fromIndex + index);
        size--;
        toIndex--;
        return result;
    }

    /**
     * Removes the first occurrence of the specified element from this sublist, if it is present (optional operation).
     * @param o
     * @return true if this sublist contained the specified element
     */

    @Override
    public boolean remove(Object o){
        int index = indexOf(o);
        if(index == -1)
            return false; //element isn't present in the sublist

        remove(index);  //element is present in the sublist, it has to be removed
        return true;
    }


    /**
     * Removes from this sublist all of its element that are contained in the specified collection (optional operation)
     * @param c
     * @return true if this sublist changed as a result of the call
     */
    public boolean removeAll(Collection<?> c){
        Object[] arr = c.toArray();
        int oldSize = size();

        for(int i = 0 ; i< arr.length; i++)
            remove(arr[i]);

        return(size()!=oldSize);
    }



    /**
     * Removes the elements from this sublist between removeFrom (inclusive), and removeTo(exclusive) (optional operation).
     */
    private void removeBetween(int removeFrom, int removeTo){
        for(int i = removeFrom ; i<removeTo; i++)
            remove(0);  //removes the first element and shifts the others to the left
    }


    /**
     * Returns the index of the first occurrence of the specified element in this sublist, or -1 if this list does not contain the element.
     * @param o
     */

    @Override
    public int indexOf(Object o){
        if(parent.indexOf(o) == -1) //object isn't present in the list, so isn't present in the sublist
            return -1;
        return parent.indexOf(o) - fromIndex;
    }

    /**
     * Returns the index in this sublist of the last occurrence of the specified element, or -1 if this sublist does not contain this element. More formally, returns the highest index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no such index.
     * @param o
     * @return the index in this sublist of the last occurrence of the specified element, or -1 if this sublist does not contain this element.
     */

    @Override
    public int lastIndexOf(Object o){
        if(parent.indexOf(o) == -1)
            return -1;
        return (parent.lastIndexOf(o) - fromIndex);
    }


    /**
     * Compares the specified object with this sublist for equality
     * @param o object to be compared for equality with this collection
     * @return true if every element in the o list is present in this sublist in the same order
     */

    @Override
    public boolean equals(Object o){
        ListAdapter<E> other = (ListAdapter<E>)o;

        if(other.size() == size()){
            for(int i = 0 ; i < size(); i++){
                if(!get(i).equals(other.get(i)))
                    return false;   //an element is different
            }
            return true;    //all element are equals
        }
        else
            return false;   //different sizes
    }

    /** Returns the hash code value for this list
     * @return the hash code value for this list
     */

    public int hashCode(){
        int hashCode = 1;
        Iterator i = this.iterator();
        while (i.hasNext()) {
            Object obj = i.next();
            hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
        }

        return hashCode;
    }


    /**
     * removes all of the elements of this list (optional operation).
     */

    @Override
    public void clear(){
        removeBetween(0,toIndex);
    }

    /**
     * Tests if the specified object appears in this list
     * @param o
     * @return true if specified object appears in this list
     * @throws NullPointerException if o is null
     */

    @Override
    public boolean contains(Object o){
        if(o == null)
            throw new NullPointerException();

        for(int i  = 0 ; i<size() ; i++) {  //cycles through the sublist
            if (get(i).equals(o))
                return true;    //element found
        }
        return false;
    }


    /**
     * Appends all of the elements in the specified collection to the end of this sublist, in the order that they are returned by the specified collection's iterator (optional operation).
     * @param c
     * @return true if this sublist changed as a result of the call
     */
    @Override
    public boolean addAll(Collection<? extends E> c){
        int oldSize = size();
        Iterator<? extends E> iterator = c.iterator();

        while(iterator.hasNext())
            add(iterator.next());

        return (size()!=oldSize);
    }

    /**
     * Inserts all of the elements in the specified collection into this sublist at the specified position (optional operation). The index is relative to the sublist
     * @param index
     * @param cl
     * @return true if this list changed as a result of the call
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> cl) {
        int oldSize = size();
        Iterator<? extends E> iterator = cl.iterator();
        int i = 0;

        while(iterator.hasNext()) {
            add(index + i, iterator.next());
            i++;
        }

        return (size()!=oldSize);

    }

    /**
     * Returns true if this sublist contains all of the elements of the specified collection.
     * @param  c collection to be checked for containment in this collection
     * @return
     * @throws NullPointerException - if the specified collection is null.
     */

    @Override
    public boolean containsAll(Collection<?> c){
        if(c == null)
            throw new NullPointerException();

        Object[] arr = c.toArray();

        for(int i = 0; i<arr.length; i++){
            if(!contains(arr[i]))
                return false;
        }
        return true;
    }



    /**
     * Retains only the elements in this sublist that are contained in the specified collection (optional operation). In other words, removes from this sublist all the elements that are not contained in the specified collection.
     * @param c
     * @return true if this sublist changed as a result of the call.
     */

    @Override
    public boolean retainAll(Collection<?> c){
        int oldSize = size();
        int newSize = 0;

        if(c.size() == 0){
            clear();
        }

        else{
            Vector<E> tempList = new Vector<>();
            Object[] arr = c.toArray();

            for(int i = 0 ; i < arr.length; i++){   //cycles through the collection
                //if an element of the collection is contained in the sublist, the element is added in the tempList
                if(contains(arr[i])){
                    tempList.add((E)arr[i]);
                    newSize++;
                }
            }

            parent.v = tempList;   //overwrite the current sublist with the tempList created
            size = newSize;
        }

        return (newSize!=oldSize);
    }



    /**
     * Returns a view of the portion of this sublist between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @param fromIndex
     * @param toIndex
     * @return a view of the portion of this sublist between the specified fromIndex, inclusive, and toIndex, exclusive.
     * @throws IndexOutOfBoundsException - for an illegal endpoint index value
     */

    public List<E> subList(int fromIndex, int toIndex){

        if(fromIndex < 0 || toIndex > size() || fromIndex > toIndex)
            throw new IndexOutOfBoundsException();

        SubList<E> list = new SubList<>(this, fromIndex,toIndex);
        return list;
    }



    /**
     * Return the iterator of the sublist
     * @return the iterator of the sublist
     */

    public Iterator<E> iterator(){
        return new Iterator<E>() {
            int curr = 0;

            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                return get(curr++);
            }
        };
    }


    /**
     * Returns a list iterator over the elements in this sublist (in proper sequence).
     * @return listIterator
     */
    public ListIterator<E> listIterator(){
        return new ListIterator<E>() {
            int curr = 0;
            boolean precSet = false;
            boolean nextSet = false;


            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                nextSet = true;
                precSet = false;
                return(get(curr++));
            }

            @Override
            public boolean hasPrevious() {
                if(curr > 0)
                    return true;
                return false;
            }

            @Override
            public E previous() {
                if(!hasPrevious())
                    throw new NoSuchElementException();
                precSet = true;
                nextSet = false;
                return(get(curr--));
            }

            @Override
            public int nextIndex() {
                return(curr);
            }

            @Override
            public int previousIndex() {
                return(curr-1);
            }

            @Override
            public void remove() {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    SubList.this.remove(curr-1);
                    nextSet=false;
                }
                else if(precSet){
                    SubList.this.remove(curr+1);
                    precSet = false;
                }
            }

            @Override
            public void set(E e) {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    SubList.this.set(curr-1,e);
                    nextSet=false;
                } else if (precSet) {
                    SubList.this.set(curr+1,e);
                    precSet=false;
                }
            }

            @Override
            public void add(E e) {
                v.insertElementAt(e,curr++);
                nextSet = false;
                precSet = false;
            }
        };
    }


    /**
     * Returns a list iterator over the elements in this sublist (in proper sequence), starting at the specified position in the sublist.
     * @param index
     * @return listIterator
     */

    public ListIterator<E> listIterator(int index){
        return new ListIterator<E>() {
            int curr = index;   //listIterator starting from the specified index
            boolean precSet = false;
            boolean nextSet = false;


            @Override
            public boolean hasNext() {
                if(curr == size())
                    return false;
                return true;
            }

            @Override
            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                nextSet = true;
                precSet = false;
                return(get(curr++));
            }

            @Override
            public boolean hasPrevious() {
                if(curr > 0)
                    return true;
                return false;
            }

            @Override
            public E previous() {
                if(!hasPrevious())
                    throw new NoSuchElementException();
                precSet = true;
                nextSet = false;
                return(get(curr--));
            }

            @Override
            public int nextIndex() {
                return(curr);
            }

            @Override
            public int previousIndex() {
                return(curr-1);
            }

            @Override
            public void remove() {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    SubList.this.remove(curr-1);
                    nextSet=false;
                }
                else if(precSet){
                    SubList.this.remove(curr+1);
                    precSet = false;
                }
            }

            @Override
            public void set(E e) {
                if(!(nextSet&&precSet))
                    throw new IllegalStateException();
                if(nextSet){
                    SubList.this.set(curr-1,e);
                    nextSet=false;
                } else if (precSet) {
                    SubList.this.set(curr+1,e);
                    precSet=false;
                }
            }

            @Override
            public void add(E e) {
                v.insertElementAt(e,curr++);
                nextSet = false;
                precSet = false;
            }
        };
    }


    /**
     * Returns the list as an object array.
     */
    @Override
    public Object[] toArray(){
        Object[] result = new Object[size()];

        for(int i = 0 ;i<size(); i++)
            result[i] = get(i);

        return result;
    }

}
