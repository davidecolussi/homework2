package com.company;
import org.junit.Test;

import java.awt.desktop.ScreenSleepEvent;
import java.util.Iterator;

import static org.junit.Assert.*;

public class SetAdapterTest<E> {
    @Test
    public void testSizeIsEmpty(){
        SetAdapter<E> set = new SetAdapter<>();
        assertEquals(0,set.size());
        assertTrue(set.isEmpty());
    }

    @Test
    public void testContains(){
        SetAdapter<E> set = getNewPopulatedSet();
        assertTrue(set.contains("prova"));
        assertTrue(set.contains("abc"));
        assertTrue(set.contains("cde"));
    }



    @Test
    public void testAdd1(){  //check add behaves correctly, new element is added to the set
        SetAdapter<E> set = getNewPopulatedSet();
        set.add((E)"nuovo");
        assertTrue(set.contains("nuovo"));
        assertEquals(4,set.size());
    }

    @Test
    public void testAdd2(){  //check add behaves correctly, new element is added to the set (duplicated)
        SetAdapter<E> set = getNewPopulatedSet();
        set.add((E)"abc");  //abc is already present in the set, size should remain the same as before

        assertEquals(3,set.size());
    }

    @Test(expected = NullPointerException.class)
    public void testAdd3(){  //check add behaves correctly, null element shall not be added to the set
        SetAdapter<E> set = getNewPopulatedSet();
        Object o = null;
        set.add((E)o);  //should throw exception
    }

    @Test
    public void testRemove1(){   //check remove(obj) behaves correctly, removing an element present in the set
        SetAdapter<E> set = getNewPopulatedSet();
        boolean result = set.remove("prova");
        assertTrue(result);   //element was contained in the set
        assertEquals(2,set.size());
    }

    @Test
    public void testRemove2(){   //check remove(obj) behaves correctly, removing an element not present in the set
        SetAdapter<E> set = getNewPopulatedSet();
        boolean result = set.remove("differente");
        assertFalse(result);   //set does not contain the element "differente"
        assertEquals(3,set.size()); //size remains the same
    }

    @Test
    public void testAddAll1(){  //check addAll behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"nuovo0");
        coll.add((E)"nuovo1");

        boolean result = set.addAll(coll);
        assertTrue(result);
        assertTrue(set.contains("nuovo0"));
        assertTrue(set.contains("nuovo1"));
    }

    @Test
    public void testAddAll2(){  //check addAll behaves correctly, empty collection
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();   //empty collection

        boolean result = set.addAll(coll);  //set should not change
        assertFalse(result);
    }

    @Test
    public void testAddAll3(){  //check addAll behaves correctly, empty collection
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();   //empty collection

        boolean result = set.addAll(coll);  //set should not change
        assertFalse(result);
    }

    @Test
    public void testContainsAll1(){  //check containsAll behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"prova");
        coll.add((E)"abc");

        assertTrue(set.containsAll(coll));
    }

    @Test
    public void testContainsAll2(){  //check containsAll behaves correctly, containsAll should return false
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"differente");

        assertFalse(set.containsAll(coll));
    }

    @Test
    public void testContainsAll3(){  //check containsAll behaves correctly, empty collection should return true
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>(); //empty collection

        assertTrue(set.containsAll(coll));
    }

    @Test(expected = NullPointerException.class)
    public void testContainsAll4(){  //check containsAll behaves correctly, null collection should throw NullPointerExcpetion
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = null;
        set.containsAll(coll);  //throw exc
    }

    @Test
    public void testContainsAll5(){  //check containsAll behaves correctly, collection of the same size buf different elements
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"prova");
        coll.add((E)"abc");
        coll.add((E)"differente");

        assertFalse(set.containsAll(coll));
    }

    @Test
    public void testRemoveAll(){    //check removeAll behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"prova");
        coll.add((E)"abc");

        boolean result = set.removeAll(coll);
        assertTrue(result);
        assertFalse(set.contains("prova"));
        assertFalse(set.contains("abc"));
    }
    //all other tests to removeAll are redundant of addAll and containsAll


    @Test
    public void testEquals1(){  //check equals behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        SetAdapter<E> set2 = getNewPopulatedSet();


        assertTrue(set.equals(set2));
    }

    @Test
    public void testEquals2(){  //check equals behaves correctly, return false because obj is not an instance of Set
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();   //coll is a listAdapter, not a set
        coll.add((E)"prova");
        coll.add((E)"abc");
        coll.add((E)"cde");


        assertFalse(set.equals(coll));
    }

    @Test
    public void testEquals3(){  //check equals behaves correctly, return false set1 and set2 have equal size, but different elements
        SetAdapter<E> set = getNewPopulatedSet();
        SetAdapter<E> set2 = getNewPopulatedSet();
        set2.remove("abc");
        set2.add((E)"nuovo");

        assertFalse(set.equals(set2));
    }

    @Test
    public void testHashCode(){ // check s1.equals(s2) implies that s1.hashCode()==s2.hashCode() for any two sets s1 and s2
        SetAdapter<E> s1 = getNewPopulatedSet();
        SetAdapter<E> s2 = getNewPopulatedSet();

        assertTrue(s1.equals(s2));
        assertEquals(s1.hashCode(),s2.hashCode());
    }

    @Test
    public void testToArray(){  //check toArray behaves correctly
        SetAdapter<E> s1 = getNewPopulatedSet();
        Object[] arr = s1.toArray();

        assertEquals(arr.length,s1.size());
        for(int i = 0 ; i<s1.size(); i++)
            assertTrue(s1.contains(arr[i]));

    }

    @Test
    public void testRetainAll1(){   //check retainAll behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();
        coll.add((E)"prova");

        boolean result = set.retainAll(coll);   //retains only "prova" in the set

        assertTrue(result);
        assertEquals(1,set.size());
        assertFalse(set.contains("abc"));
        assertFalse(set.contains("cde"));
        assertTrue(set.contains("prova"));
    }

    @Test
    public void testRetainAll2(){   //check retainAll behaves correctly (empty collection)
        SetAdapter<E> set = getNewPopulatedSet();
        Collection<E> coll = new ListAdapter<>();   //empty collection

        boolean result = set.retainAll(coll);   //retains nothing in the set

        assertTrue(result);
        assertEquals(0,set.size());
    }

    @Test
    public void testIterator(){ //check iterator behaves correctly
        SetAdapter<E> set = getNewPopulatedSet();
        Iterator<E> iterator = set.iterator();
        Object[] arr = set.toArray();
        int i = 0;

        while(iterator.hasNext()){
            assertEquals(arr[i],iterator.next());
            i++;
        }
    }


    //metodo utile
    public SetAdapter<E> getNewPopulatedSet(){
        SetAdapter<E> set = new SetAdapter<>();
        set.add((E)"prova");
        set.add((E)"abc");
        set.add((E)"cde");
        return set;
    }

    //metodo utile
    public void print(SetAdapter<E> set){
        Object[] arr = set.toArray();
        for(int i = 0 ; i < set.size(); i++)
            System.out.println("pos " + i + ": " + arr[i]);
    }
}
